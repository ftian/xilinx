#include <string>
#include <vector>
#include <cstdio>
#include <iostream>

#include <sys/time.h>

#include "xliw.h"
#include "xcl2.hpp" 

#define NPP 64
#define PPSZ 19
#define PPNB (PPSZ * 8)


static void cpu_pip(xliw_t* xliw, xliw_t *result, int resultsz) 
{

	int *prinfo = (int *) aligned_alloc(4096, 4096);

	// return result start after first xliw_t, size = 32, or 8 * sizeof(int)
	int retstart = 8;

	double *p0 = (double *) xliw;
	
	for (int i = 0; i < 128; i++) {
		prinfo[i*4] = 0;
		prinfo[i*4 + 1] = 0;
		prinfo[i*4 + 2] = 0;
		prinfo[i*4 + 3] = 0;

		if (i < xliw->rcnt) {
			xliw_region_t *r = xliw_region(xliw, i);
			if (r->length == 0) {
				/* empty region */
				continue;
			}

			double *pp = (double *) xliw_region_data(xliw, i);
			prinfo[i*4] = pp - p0;
			prinfo[i*4 + 1] = r->cnt;
			prinfo[i*4 + 2] = retstart;
			prinfo[i*4 + 3] = 0;
			retstart += r->cnt;
		}
	}

	int *pr = prinfo;
	double *ppin = (double *) xliw;
	int *retin = (int *) result;

	// LOOP.  
	for (int idx = 0; idx < 128; idx++) {

		int ppstart = pr[idx*4];
		int n_elems = pr[idx*4 + 1]; 
		int retstart = pr[idx*4 + 2]; 
		/* int dummy = pr[idx*4 + 3]; */ 

		const double *pp = &ppin[ppstart];
		int *ret = &retin[retstart];

		double gbuf[128][NPP * PPSZ];
		int gwn[128][NPP];

		double *buf = gbuf[idx];
		int *wn = gwn[idx];

		for (int n = 0; n < n_elems; n+=NPP) {
			int len = NPP;
			if (n + len > n_elems) {
				len = n_elems - n;
			}

			/* COPY IN */
			int nstart = n * PPSZ;
			for (int i = 0; i < len * PPSZ; i++) {
				buf[i] = pp[nstart + i];
			}

			/* Run PtInPoly for len inputs */
			for (int i = 0; i < len; i++) {
				int ptpos = i * PPSZ + 2;
				int plypos = i * PPSZ + 5;
				double *pt = buf + ptpos; 
				double *ply = buf + plypos;
				wn[i] = 0;

				for (int j = 0; j < 6; j++) {		/* edge from v[i] to v[i+1] */
					int v0x = j*2;
					int v0y = v0x + 1;
					int v1x = v0y + 1;
					int v1y = v1x + 1;
					if (ply[v0y] <= pt[1]) {		/* v0.y <= p.y */
						if (ply[v1y] > pt[1]) {		/* upward crossing */
							double isLeft = 
								(ply[v1x] - ply[v0x]) * (pt[1] - ply[v0y]) 
								- (pt[0] - ply[v0x]) * (ply[v1y] - ply[v0y]); 
							if (isLeft > 0) {
								++wn[i];
							}
						}
					} else {						// v.y > p.y
						if (ply[v1y] <= pt[1]) {
							double isLeft = 
								(ply[v1x] - ply[v0x]) * (pt[1] - ply[v0y]) 
								- (pt[0] - ply[v0x]) * (ply[v1y] - ply[v0y]); 
							if (isLeft < 0) {
								--wn[i];
							}
						}
					}
				}
			}

			/* COPY result out. */
			for (int i = 0; i < len; i++) {
				ret[n + i] = wn[i]; 
			}
		}
	}
}

#define N_SUB_KERNEL 16 
struct xliw_fpga_ctxt {
	std::vector<cl::Device> devices;
	cl::Context *context; 
	cl::Program *program;
	cl::Program::Binaries xclBins;
	cl::CommandQueue *q;
	cl::Kernel *krnl;
};

static xliw_fpga_ctxt g_ctxt;

int xliw_init_fpga(int ndev)
{
	cl_int err;

	g_ctxt.devices = xcl::get_xil_devices();
	cl::Device device = g_ctxt.devices[0];
	OCL_CHECK(err, g_ctxt.context = new cl::Context(g_ctxt.devices[0], NULL, NULL, NULL, &err));

	g_ctxt.q = new cl::CommandQueue(*g_ctxt.context, device);

	std::string device_name = device.getInfo<CL_DEVICE_NAME>(&err);
    std::cout << "xliw_init_fpga: Found Device=" << device_name.c_str() << std::endl;

	std::string binaryFile = xcl::find_binary_file(device_name, "krnl_pip");
    std::cout << "xliw_init_fpga: binaryFile=" << binaryFile << std::endl; 

	g_ctxt.xclBins = xcl::import_binary_file(binaryFile); 
	g_ctxt.devices.resize(1);
	OCL_CHECK(err, g_ctxt.program = new cl::Program((*g_ctxt.context), g_ctxt.devices, g_ctxt.xclBins, NULL, &err));

	g_ctxt.krnl = new cl::Kernel((*g_ctxt.program), "krnl_pip", &err);
	return 0;
}

int xliw_deinit_fpga()
{
	delete g_ctxt.q;
	delete g_ctxt.krnl;
	delete g_ctxt.program;
	delete g_ctxt.context;

	g_ctxt.devices.clear(); 
	return 0;
}

static void fpga_pip(xliw_t *xliw, xliw_t *result, int resultsz) 
{
	cl_int err;

	int *prinfo = (int *) aligned_alloc(4096, 4096);

	// return result start after first xliw_t, size = 32, or 8 * sizeof(int)
	int retstart = 8;

	double *p0 = (double *) xliw;
	
	for (int i = 0; i < 128; i++) {
		prinfo[i*4] = 0;
		prinfo[i*4 + 1] = 0;
		prinfo[i*4 + 2] = 0;
		prinfo[i*4 + 3] = 0;

		if (i < xliw->rcnt) {
			xliw_region_t *r = xliw_region(xliw, i);
			if (r->length == 0) {
				/* empty region */
				continue;
			}

			double *pp = (double *) xliw_region_data(xliw, i);
			prinfo[i*4] = pp - p0;
			prinfo[i*4 + 1] = r->cnt;
			prinfo[i*4 + 2] = retstart;
			prinfo[i*4 + 3] = 0;
			retstart += r->cnt;
		}
	}

	OCL_CHECK(err, cl::Buffer buf_pr(*g_ctxt.context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, (size_t )4096, (void *) prinfo, &err));
	OCL_CHECK(err, cl::Buffer buf_x(*g_ctxt.context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, (size_t )16*1024*1024, (void *) xliw, &err));
	OCL_CHECK(err, cl::Buffer buf_r(*g_ctxt.context, CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY, (size_t)resultsz, (void *) result, &err));

	std::vector<cl::Memory> inBufVec, outBufVec;
	inBufVec.push_back(buf_pr);
	inBufVec.push_back(buf_x);
	outBufVec.push_back(buf_r);

	// Copy in
    OCL_CHECK(err, err = g_ctxt.q->enqueueMigrateMemObjects(inBufVec,0/* 0 means from host*/));

    //set the kernel Arguments
    int narg=0;
    OCL_CHECK(err, err = g_ctxt.krnl->setArg(narg++, buf_pr)); 
    OCL_CHECK(err, err = g_ctxt.krnl->setArg(narg++, buf_x)); 
    OCL_CHECK(err, err = g_ctxt.krnl->setArg(narg++, buf_r)); 

    //Launch the Kernel
    OCL_CHECK(err, err = g_ctxt.q->enqueueNDRangeKernel(*g_ctxt.krnl, cl::NullRange, cl::NDRange(128), cl::NDRange(128))); 

	// Copy out.
    OCL_CHECK(err, err = g_ctxt.q->enqueueMigrateMemObjects(outBufVec,CL_MIGRATE_MEM_OBJECT_HOST));
    g_ctxt.q->finish();
}

/*
 * Execute GIS_INTERSECTS
 * Input xliw consists of rcnt "regions", each region has a array
 * of "geometry types"
 */
int xliw_execute_gis_intersects(xliw_t *xliw, int oncpu, int *exec_us)
{
	struct timeval tv0, tv1;
	if (exec_us) { 
		gettimeofday(&tv0, 0);
		*exec_us = 0;
	}

	/* result is just an array of bool */
	int32_t resultsz = exx_align(sizeof(xliw_t) + xliw->hcnt * sizeof(int), 4096); 
	xliw_t *result = (xliw_t *) aligned_alloc(4096, resultsz); 
	if (!result) {
		/* OOM */
		return -1;
	}
	memset(result, 0, resultsz); 

	result->len = resultsz;
	result->xliw = XLIW_I_GIS_RESULT; 
	result->hcnt = xliw->hcnt;

	/* 
	 * Notice the loop, there should be no hazzard at all if we do 
	 * not use a loop, but use rcnt threads instead.
	 */
	if (oncpu == EXEC_ON_CPU) {
		cpu_pip(xliw, result, resultsz); 
	} else {
		fpga_pip(xliw, result, resultsz); 
	}

	/* 
	 * FPGA_PIP will copy out memory from device.   Overwrite result. 
	 */
	result->len = resultsz;
	result->xliw = XLIW_I_GIS_RESULT; 
	result->hcnt = xliw->hcnt;


	/* Copy it back, as the API says return same buffer. */
	memcpy(xliw, result, resultsz);
	xliw_free(result);

	if (exec_us) {
		gettimeofday(&tv1, 0);
		*exec_us = (tv1.tv_sec - tv0.tv_sec) * 1000000 + tv1.tv_usec - tv0.tv_usec;
	}
	return 0;
}

