/* 
 * Implemented API invoked by xliw client.
 * Note: we assume that the client calls these functions is single threaded 
 */
#define _POSIX_C_SOURCE (200112L)
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/queue.h>
#include <netinet/in.h>
#include <assert.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdbool.h>

#include "xliw.h"
#include "xliwagent.h"

struct xliw_client_req_s;
TAILQ_HEAD(xliw_client_req_tailqhead_t, xliw_client_req_s);

typedef struct xliw_client_req_s {
    int32_t           req_id;    // request id
    char             *response;  // pointer to response
    sem_t             resp_ready; // wait for response is ready. init to 0

    TAILQ_ENTRY(xliw_client_req_s) entries;
} xliw_client_req_t;

typedef struct xliw_client_data_s {
    int               fd;
    int32_t           next_req_id;
    pthread_t         read_thread;
    int               read_error;
    bool              destroy;     // set to true to tell read_thread to destroy

    // mutex to lock the pending request queue
    pthread_mutex_t   mutex;

    // queue to hold all pending requests 
    struct xliw_client_req_tailqhead_t reqhead;    
} xliw_client_data_t;

static xliw_client_data_t *s_xliw_client_data = NULL;

static void free_client_req(xliw_client_req_t *req);
static void free_client_data(xliw_client_data_t *data);
static int send_data(int fd, char *buf, int len);
static int recv_data(int fd, char *buf, int len, bool *cancelled );
static void *xliw_client_read_thread_start(void *arg);

// return 0 if succeed,otherwise -<error_number>
int xliw_connect(char *unix_path)
{
    if(s_xliw_client_data != NULL)
        return 0;

    // create blocking socket as we only have one socket there
    struct sockaddr_un uaddr;
    int fd, ret;

    fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if(fd < 0) {
        return XLIW_ERR_SOCK_CREATE;
    }

    // set server address
    memset(&uaddr, 0, sizeof(struct sockaddr_un));
    uaddr.sun_family = AF_UNIX;
    strcpy(uaddr.sun_path, unix_path);
    ret = connect(fd, (struct sockaddr *) &uaddr, sizeof(struct sockaddr_un));
    if( ret != 0) {
        close(fd);
        return XLIW_ERR_SOCK_CONN;
    }

    // create xliw client object
    s_xliw_client_data = calloc(1, sizeof(xliw_client_data_t));
    if(NULL == s_xliw_client_data) {
        close(fd);
        return XLIW_ERR_NO_MEMORY;
    }
    s_xliw_client_data->fd = fd;
    s_xliw_client_data->next_req_id = 1;
    s_xliw_client_data->destroy = false;
    TAILQ_INIT(&(s_xliw_client_data->reqhead));

    if(pthread_mutex_init(&(s_xliw_client_data->mutex), NULL) != 0) {
        close(fd);
        free(s_xliw_client_data);
        s_xliw_client_data = NULL;
        return XLIW_ERR_MUTEX_CREATE;
    }

    // start reading thread
    if ((ret = pthread_create( &(s_xliw_client_data->read_thread),
                        0,
                        xliw_client_read_thread_start,
                        s_xliw_client_data)) != 0 ) {
        close(fd);
        free(s_xliw_client_data);
        s_xliw_client_data = NULL;
        return XLIW_ERR_THREAD_CREATE;
    }

    return 0;
}

void xliw_disconnect()
{
    if(NULL == s_xliw_client_data)
        return;

    // tell reading thread to destroy itself
    s_xliw_client_data->destroy = true;

    // wait for read_thread to exit.
    pthread_join(s_xliw_client_data->read_thread, NULL);

    free_client_data(s_xliw_client_data);
    s_xliw_client_data = NULL;
}

/*
 * return 0 if succeeds, othrwise error codes
 * this function is synched until the result is recevied back.
 */
int xliw_execute_agent(xliw_t *xliw, int* exec_us) 
{
    int ret = 0;

    struct timeval tv0, tv1;
    if (exec_us) {
        gettimeofday(&tv0, 0);
    }
    
    ret = xliw_execute_agent_async(xliw, NULL);
    if( ret > 0) {
        int32_t req_id = ret;
        ret = xliw_future_get(req_id, xliw);
    }

    if (exec_us) {
        gettimeofday(&tv1, 0);
        *exec_us = tvdiff(&tv0, &tv1);
    }

    return ret;
}

/*
 * return req_id (always>0) which used for future readiness check
 * if there is error, return minus <error_number>
 */
int xliw_execute_agent_async(xliw_t *xliw, int* exec_us) 
{
    char *buf = (char*)xliw;
    int  len = XLIW_SZ;  // the size of xliw is 16M
    int  ret;

    if (xliw->xliw != XLIW_I_SHA224 && xliw->xliw != XLIW_I_SHA224_SORT) {
        /* bad instruction */
        return -100;
    }

    struct timeval tv0, tv1;
    if (exec_us) {
        gettimeofday(&tv0, 0);
    }

    //create req object
    xliw_client_req_t *req;
    req = calloc(1, sizeof(xliw_client_req_t));
    if(NULL == req) {
        return XLIW_ERR_NO_MEMORY;
    }

    if(sem_init(&(req->resp_ready), 0, 0) != 0) {
        free(req);
        return XLIW_ERR_SEM_CREATE;
    }
    req->response = NULL;
    req->req_id = s_xliw_client_data->next_req_id++;;

    // put req object to the request queue
    // we need to insert request first before sending request.
    pthread_mutex_lock(&(s_xliw_client_data->mutex));
    TAILQ_INSERT_TAIL(&(s_xliw_client_data->reqhead), req, entries);
    pthread_mutex_unlock(&(s_xliw_client_data->mutex));

    //send the data to agent
    ret = send_data(s_xliw_client_data->fd, buf, len);
    if(0 == ret) {
        //send req_id to agent
        ret = send_data(s_xliw_client_data->fd, (char*)&(req->req_id), sizeof(req->req_id));
        if(0 == ret) {
            if (exec_us) {
                gettimeofday(&tv1, 0);
                *exec_us = tvdiff(&tv0, &tv1);
            }
            return (int)req->req_id;
        }
    }

    // there is error. remove the request
    pthread_mutex_lock(&(s_xliw_client_data->mutex));
    TAILQ_REMOVE(&(s_xliw_client_data->reqhead), req, entries);
    pthread_mutex_unlock(&(s_xliw_client_data->mutex));
    free_client_req(req);
    return ret;
}

// return 0 if readiness check succeeds, othre return -<error_number>
// is_ready is set to true/false if succeeds
int xliw_future_is_ready(int32_t req_id, bool *is_ready)
{
    pthread_mutex_lock(&(s_xliw_client_data->mutex));
    xliw_client_req_t *req = TAILQ_FIRST(&(s_xliw_client_data->reqhead));
    while(NULL != req) {
        if( req->req_id == req_id) {
            if(NULL == req->response) {
                *is_ready = false;
            } else {
                *is_ready = true;
            }
            break;
        }
        req = TAILQ_NEXT(req, entries);
    }
    pthread_mutex_unlock(&(s_xliw_client_data->mutex));

    // invalid req id
    if(NULL == req)
        return XLIW_ERR_INVALID_REQ_ID; 
    else {
        if(!(*is_ready) && 0 != s_xliw_client_data->read_error)
            return s_xliw_client_data->read_error;
        return 0;
    }
}

/* return 0 if succeeds. otherwise minus error_number
 * This function will Wait till the response to the req_id is ready 
 * and fill the xliw with result
 */
int xliw_future_get(int32_t req_id, xliw_t *xliw)
{
    int ret;
    xliw_client_req_t *req = NULL;
    bool               wait_for_resp = false;

    // find the request object
    pthread_mutex_lock(&(s_xliw_client_data->mutex));
    req = TAILQ_FIRST(&(s_xliw_client_data->reqhead));
    while(NULL != req) {
        xliw_client_req_t *next = TAILQ_NEXT(req, entries);
        if( req->req_id == req_id) {
            if(req->response) {
                // remove the request object out of the tailq
                TAILQ_REMOVE(&(s_xliw_client_data->reqhead), req, entries);
            } else {
                wait_for_resp = true;
            }
            break;
        }
        req = next;
    }
    pthread_mutex_unlock(&(s_xliw_client_data->mutex));

    // invalid req id
    if( NULL == req) {
        return XLIW_ERR_INVALID_REQ_ID;
    }
    // wait for the response ready
    if( wait_for_resp) {
        // use sem_timedwait to periodically exit and check if there 
        // is error in read thread. if there is read error, 
        // we should not continue
        for(;;) {
            struct timespec ts;
            clock_gettime(CLOCK_REALTIME, &ts);
            ts.tv_nsec += 100000000;
            if( ts.tv_nsec >= 1000000000) {
                ts.tv_sec++;
                ts.tv_nsec -= 1000000000;
            }
            ret = sem_timedwait(&(req->resp_ready), &ts);

            // the reader thread may have error occured
            if( 0 != s_xliw_client_data->read_error) {
                return s_xliw_client_data->read_error;
            }
            if( ret == -1 ) {
                if( errno == EINTR || errno == ETIMEDOUT || errno == EAGAIN)
                    continue;
                else {
                    printf("sem_timedwait error %s\n", strerror(errno));
                    return XLIW_ERR_SEM_WAIT;
                }
            } else {
                break;
            }
        }

        // remove the request object from the tailq
        pthread_mutex_lock(&(s_xliw_client_data->mutex));
        TAILQ_REMOVE(&(s_xliw_client_data->reqhead), req, entries);
        pthread_mutex_unlock(&(s_xliw_client_data->mutex));
    }

    memcpy(xliw, req->response, XLIW_SZ);
    free_client_req(req);
    if(XLIW_I_HASH_RESULT_ERR == xliw->xliw) {
        return XLIW_ERR_AGENT;
    }

    return 0;
}

/* main function for read thread */
static void *xliw_client_read_thread_start(void *arg)
{
    xliw_client_data_t *client = (xliw_client_data_t *)arg;
    int ret;

    // set timeout of receive function so we can check whether xliw_disconnect
    // has been called.
    struct timeval  tv;
    tv.tv_sec = 0;
    tv.tv_usec= 100000; //0.5 sec
    setsockopt(client->fd, SOL_SOCKET, SO_RCVTIMEO,
                                (struct timeval *)&tv, sizeof(struct timeval));

    for(;;) {
        //read result. block read until the full msg is received.
        char *buf = (char*)malloc(XLIW_SZ);
        int len = XLIW_SZ;

        if(NULL == buf) {
            client->read_error = XLIW_ERR_NO_MEMORY;
            return NULL;
        } 

        // read data
        ret = recv_data(client->fd, buf, len, &client->destroy);
        // check destroy flag again
        if(client->destroy) {
            return NULL;
        }
        if(0 != ret)  {
            client->read_error = ret;
            return NULL;
        }
        // read req_id
        int32_t req_id;
        ret = recv_data(client->fd, (char*)&req_id, sizeof(req_id), &client->destroy);
        // check destroy flag again
        if(client->destroy) {
            return NULL;
        }
        if(0 != ret)  {
            client->read_error = ret;
            return NULL;
        }

        // find request object and append reponse to the request, wakeup the waiting
        // thread if there is.
        pthread_mutex_lock(&(client->mutex));
        xliw_client_req_t *req = TAILQ_FIRST(&(client->reqhead));
        while(NULL != req) {
            if( req->req_id == req_id) {
                if(NULL != req->response) {
                    // TODO: something wrong, same req_id has multiple response. should not happen.
                    free(buf);
                } else {
                    req->response = buf;
                    sem_post(&(req->resp_ready));
                }
                break;
            }

            req = TAILQ_NEXT(req, entries);
        }
        pthread_mutex_unlock(&(client->mutex));
    }

}

static void free_client_req(xliw_client_req_t *req)
{
    free(req->response);
    sem_destroy(&(req->resp_ready));
    free(req);
}

static void free_client_data(xliw_client_data_t *data)
{
    //close socket
    shutdown(data->fd, SHUT_RD|SHUT_WR);
    close(data->fd);

    pthread_mutex_destroy(&(data->mutex));

    // free request queue
    xliw_client_req_t *req = TAILQ_FIRST(&(data->reqhead));
    while(NULL != req) {
        xliw_client_req_t *next = TAILQ_NEXT(req, entries);
        free_client_req(req);
        req = next;
    }

    free(data);
}

//wait until all data has been sent or error
static int send_data(int fd, char *buf, int len) 
{
    while(len > 0) {
        int sent_len = send(fd, buf, len, MSG_NOSIGNAL);

        if (-1 == sent_len) {
            if ( EINTR == errno) {
                continue;
            }
            return XLIW_ERR_CONN_WRITE;
        } else if (0 == sent_len) {
            // connection is teared down by peer
            return XLIW_ERR_CONN_TEARDOWN;
        }

        len -= sent_len;
        buf += sent_len;
    }

    return 0;
}

//wait until all data has been received or error, or cancelled
static int recv_data(int fd, char *buf, int len, bool *cancelled) 
{
    //wait for the result. block read until the full msg is received.
    while(len > 0) {
        int recv_len = recv(fd, buf, len, 0);

        // check if cancelled is set
        if(*cancelled)
            return 0;

        if(-1 == recv_len) {
            if ( EINTR == errno || EAGAIN == errno || EWOULDBLOCK == errno) {
                continue;
            }
            return XLIW_ERR_CONN_READ;
        } else if (0 == recv_len) {
            // connection is teared down by peer
            return XLIW_ERR_CONN_TEARDOWN;
        }
        buf += recv_len;
        len -= recv_len;
    }
    return 0;
}

