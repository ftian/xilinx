/*
 *   xliw_agent.c
 *     local agent which receives msg from QEs, compute, then send response to QEs.
 *
*/
#define _POSIX_C_SOURCE (200112L)
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <sys/epoll.h>
#include <sys/queue.h>
#include <sys/syscall.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <assert.h>
#include <pthread.h>
#include <semaphore.h>
#include <limits.h>

#include "xliw.h"
#include "xliwagent.h"

struct xliw_worker_t;
struct xliw_mq_entry_t;
TAILQ_HEAD(xliw_msg_tailqhead_t, xliw_mq_entry_t);

#define  MAX_WORKER_NUM    8
#define  MAX_CLIENT_NUM    1024
#define MEM_ALLOC_SIZE (XLIW_SZ+sizeof(int32_t))

typedef struct xliw_client_t {
    int             conn_fd;
    bool            disconnected;
    struct xliw_worker_t  *worker;// pointer to the worker asisgned to the client

    // read buffer
    xliw_t          *xliw;  // pointer to full xliw object;
    char            *next_p;    // pointer to buffer of next recv
    size_t          recv_len;   // size of next recv request
} xliw_client_t;

typedef struct xliw_mq_entry_t {
    xliw_client_t   *client;
    xliw_t          *xliw;

    // This hold the pointers to the next and previous msg in the tail queue
    TAILQ_ENTRY(xliw_mq_entry_t) entries;
} xliw_mq_entry_t;

typedef struct xliw_worker_t {
    pthread_t       thread;
    sem_t           msgs_waited;
	int worker_nth;

    // msg queue for the worker
    TAILQ_HEAD(, xliw_mq_entry_t) qhead;
    int32_t         msg_count;
    pthread_mutex_t mutex;      // lock queue access
} xliw_worker_t;

/* Folloiwng is the for logger */
// Log Level
#define XLIW_LOG_STDERR            0
#define XLIW_LOG_EMERG             1
#define XLIW_LOG_ALERT             2
#define XLIW_LOG_CRIT              3
#define XLIW_LOG_ERR               4
#define XLIW_LOG_WARN              5
#define XLIW_LOG_NOTICE            6
#define XLIW_LOG_INFO              7
#define XLIW_LOG_DEBUG             8

#define XLIW_MAX_LOG_STR          2048
static char  *err_levels[] = {
       "",
       "emerg",
       "alert",
       "crit",
       "error",
       "warn",
       "notice",
       "info",
       "debug"
};

typedef struct xliw_agent_logger_t {
    FILE           *fp;
    int             log_level;
} xliw_logger_t;

// for read thread
static int xliw_create_add_client(int epoll_fd, int conn_fd);
static void xliw_free_client( xliw_client_t *client);
static int xliw_read_msg(xliw_client_t *client, bool read_all);
static int xliw_dispatch_msg(xliw_client_t *client, xliw_t *xliw);
static int xliw_agent_read(char *unix_path);

// for worker thread
static void* xliw_worker_thread_start( void *arg);

static int s_worker_num = 2;
static xliw_worker_t s_workers[MAX_WORKER_NUM];
static xliw_logger_t s_logger;
static uint32_t s_next_worker;

static inline uint32_t xliw_atomic_incr_uint32(uint32_t *p)
{
    return __sync_fetch_and_add(p, 1);
}

static inline uint32_t xliw_atomic_decr_uint32(uint32_t *p)
{
    return __sync_fetch_and_sub(p, 1);
}

static void xliw_agent_log_init(char *filename, int log_level)
{
    xliw_logger_t *logger = &s_logger;

    memset(logger, 0, sizeof(xliw_logger_t));
    logger->log_level = log_level;

    logger->fp = fopen(filename, "a");
    if( NULL == logger->fp ) {
       logger->fp = stderr;
    }
}

static void xliw_agent_log( int level,
                    const char *fmt,
                    ...)
{
    va_list      args;
    xliw_logger_t *logger = &s_logger;

    if (logger->log_level < level) {
         return;
    }

    char        log_str[XLIW_MAX_LOG_STR];
    char        *p;
    size_t      buf_left = XLIW_MAX_LOG_STR - 2;
    int         len;

    p = log_str;

    // add time, log_leel
    struct timeval tv;
    char   time_str[30];
    int    time_len;
    char   *year_p;

    gettimeofday(&tv, NULL);
    ctime_r(&tv.tv_sec, time_str);
    time_len = strlen(time_str)-1;
    time_str[time_len] = '\0'; // remove \n at the end
    time_str[time_len-5] = '\0';
    year_p = time_str + time_len -4;

    len = snprintf(p, buf_left, "%s.%06d %s [%s] ", time_str, (int)tv.tv_usec, year_p, err_levels[level]);
    p += len;
    buf_left -=len;

    while(len < 9) {
        *p = ' ';
        p++;
        buf_left--;
        len++;
    }

    // add process/thread id
    len = snprintf(p, buf_left, "%d#%ld: ",  getpid(), syscall(__NR_gettid));
    p += len;
    buf_left -= len;

    // add args
    va_start(args, fmt);
    len = vsnprintf(p, buf_left, fmt, args);
    va_end(args);
    p += len;
    buf_left -=len;

    // add endofline
    *p = '\n';
    p++;

    fwrite(log_str, 1, p - log_str, logger->fp);
    fflush(logger->fp);
}

/* start point of thread reading unix connections from client
 * return NULL
 */
static int xliw_agent_read(char *unix_path)
{
    struct sockaddr_un  uaddr;
    int                 listen_fd;
    int                 epoll_fd;
    struct epoll_event  ev; 
    struct epoll_event  events[MAX_CLIENT_NUM + 1];

    // create non-blocking socket
    listen_fd = socket(AF_UNIX, SOCK_STREAM|SOCK_NONBLOCK, 0);
    if(listen_fd < 0) {
        xliw_agent_log(XLIW_LOG_CRIT, "Can not open create unix socket: %s",
                                           strerror(errno));
        return -1; 
    } 


    unlink(unix_path);

    // set address 
    memset(&uaddr, 0, sizeof(struct sockaddr_un));
    uaddr.sun_family = AF_UNIX; 
    strcpy(uaddr.sun_path, unix_path);
    
    // bind then listen
    if(bind(listen_fd, (struct sockaddr *) &uaddr,
                sizeof(struct sockaddr_un)) != 0) { 
        xliw_agent_log(XLIW_LOG_CRIT, "Can not bind unix socket fd:%d unix_path:%s reason:%s",  
                listen_fd, unix_path, strerror(errno));
        close(listen_fd);
        unlink(unix_path);
        return -1;
    } 

    if(listen(listen_fd, 100) != 0) { 
        xliw_agent_log(XLIW_LOG_CRIT, "Can not listen on unix socket fd:%d unix_path:%s reason:%s", listen_fd, unix_path, strerror(errno));
        close(listen_fd);
        unlink(unix_path);
        return -1;
    }

	// change permission.  On aws, we must run this as root, but then deepgreen agent cannot connect.
	if (fchmod(listen_fd, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH) != 0) {
        xliw_agent_log(XLIW_LOG_CRIT, "Can not change permision on unix socket fd:%d unix_path:%s reason:%s", listen_fd, unix_path, strerror(errno));
        close(listen_fd);
        unlink(unix_path);
        return -1;
    }

    xliw_agent_log(XLIW_LOG_INFO, "Listen to unix socket fd:%d unix_path:%s successfully", listen_fd, unix_path);
    // create epoll
    epoll_fd = epoll_create1(0);
    if ( -1 == epoll_fd ) { 
        xliw_agent_log(XLIW_LOG_CRIT, "Function %s failed reason:%s", "epoll_create1", strerror(errno));
        close(listen_fd);
        unlink(unix_path);
        return -1;
    }

    // add listen socket to epoll
    ev.events = EPOLLIN;
    ev.data.fd = listen_fd;
    if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, listen_fd, &ev) == -1 ) { 
        xliw_agent_log(XLIW_LOG_CRIT, "Failed to add listen_fd %d to epoll. Reason:%s", 
                                      listen_fd, strerror(errno));
        close(epoll_fd);
        close(listen_fd);
        unlink(unix_path);
        return -1;
    }

    // wait for connection/data ready using epoll. three senario
    // * new connection from client
    // * data is ready to read in connection fd
    // * connection failure is detected
    for (;;) {
        int fds = epoll_wait(epoll_fd, events, MAX_CLIENT_NUM + 1, -1); 

        if ( -1 == fds ) {
            if ( EINTR == errno) {
                continue;
            }

            xliw_agent_log(XLIW_LOG_CRIT, "Function epoll_wait failed reason:%s", strerror(errno));
            close(epoll_fd);
            close(listen_fd);
            unlink(unix_path);
            return -1;
        } 
                             
        for (int n = 0; n < fds; ++n) { 
            struct epoll_event myev = events[n];

            if (myev.data.fd == listen_fd) { 
                // listen socket is triggered
                int conn_fd = accept(listen_fd, NULL, NULL); 

                if (-1 == conn_fd) { 
                    if (EAGAIN != errno && EWOULDBLOCK != errno) {
                        xliw_agent_log(XLIW_LOG_ERR, "Accept failed. Reason:%s", strerror(errno));
                        close(epoll_fd);
                        close(listen_fd);
                        unlink(unix_path);
                        return -1;
                    }
                    continue;
                }

                // create interface object and add it to interface table
                xliw_agent_log(XLIW_LOG_DEBUG, "Accept a unix connection fd:%d", conn_fd);
                if (xliw_create_add_client(epoll_fd, conn_fd) != 0) {
                    close(conn_fd);
                    continue;
                } 
            } else {
                xliw_client_t *client = (xliw_client_t *)(myev.data.ptr);
                int         fd = client->conn_fd;
                bool        need_close = false;

                if (EPOLLIN & myev.events) {
                    // Data ready to be read in socket 
                    if (xliw_read_msg(client, false) == -1) {
                        // message read error, should close the connection
                        need_close = true;
                    }
                }

                if( (~EPOLLIN) & myev.events) {
                    // connection is closed by peer.
                    xliw_agent_log(XLIW_LOG_DEBUG, "Connection fd:%d teared down", fd);
                    need_close = true;
                }

                if( need_close) {
                    // should read all pending data in the connection first
                    if (EPOLLIN & myev.events) {
                        xliw_read_msg(client, true);
                    }

                    // Close connection.
                    //    - remove the fd from epoll
                    //    - set client's disconncted to true. 
                    epoll_ctl(epoll_fd, EPOLL_CTL_DEL, fd, NULL);
                    client->disconnected = true;

					// will close connection.
					xliw_dispatch_msg(client, 0);
                } // if( need_close)
            }
        }// for fds

    } // for(;;)

    // should never be reached
    close(epoll_fd);
    close(listen_fd);
    unlink(unix_path);
    return -1;
}

static int xliw_create_add_client(int epoll_fd, int conn_fd)
{
    xliw_client_t *client;
    struct epoll_event  ev; 

    client = malloc(sizeof(xliw_client_t));
    memset(client, 0, sizeof(xliw_client_t));
    if( NULL == client) {
        xliw_agent_log(XLIW_LOG_ERR, "No enough memeory on %s size %d", 
                            "xliw_client_t", sizeof(xliw_client_t));
        return -1;
    }

    client->conn_fd = conn_fd; 
    client->disconnected = false;
    client->worker = NULL;
    client->xliw = NULL; 
	if (posix_memalign((void **)&client->xliw, XLIW_ALIGN, MEM_ALLOC_SIZE) || 
			0 == client->xliw) {
		xliw_agent_log(XLIW_LOG_ERR, "No enough memeory on %s size %d", 
				"xliw_t", MEM_ALLOC_SIZE); 
		return -1;
	}

    client->next_p = (char*)client->xliw;
    client->recv_len = MEM_ALLOC_SIZE; 

    // create read buffer for the connection
    // add the connected socket to epoll
    ev.events = EPOLLIN; 
    ev.data.ptr = client; 

    if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, conn_fd, &ev) == -1) { 
        xliw_agent_log(XLIW_LOG_ERR, "Failed to add client fd %d to epoll. Reason:%s", 
                                      conn_fd, strerror(errno));
        xliw_free_client(client);
        return -1;
    } 
    return 0;
}

// Free the read buffer
static void xliw_free_client( xliw_client_t *client)
{
    if( NULL == client) {
        return;
	}

	xliw_free(client->xliw);

    if(client->conn_fd >= 0) {
        close(client->conn_fd);
    }
    free(client);
}

static int xliw_dispatch_msg(xliw_client_t *client, xliw_t *xliw)
{
	// printf("Dispatch msg to xliw %p.\n", xliw);
    xliw_mq_entry_t *mq_entry = calloc(1, sizeof(xliw_mq_entry_t));

    if(NULL == mq_entry) {
        xliw_agent_log(XLIW_LOG_ERR, "No enough memeory on %s size %d", 
                            "xliw_mq_entry_t", sizeof(xliw_mq_entry_t));
        return -1;
    }
    mq_entry->xliw = xliw;
    mq_entry->client = client;

	xliw_worker_t *worker = client->worker;
	if (!worker) {
		uint32_t next = xliw_atomic_incr_uint32(&s_next_worker);
		worker = &s_workers[next % s_worker_num];
		client->worker = worker;
	}

    pthread_mutex_lock(&(worker->mutex));
    TAILQ_INSERT_TAIL(&(worker->qhead), mq_entry, entries);
    worker->msg_count++;
    pthread_mutex_unlock(&(worker->mutex));

    sem_post(&(worker->msgs_waited));
    return 0;
}

// read data from interface using non-blocking. if a full msg is
// received, put it to queue for handle_msg thread
// success return 0, otherwise return -1
static int xliw_read_msg(xliw_client_t *client, bool read_all)
{
    int fd;
    ssize_t len;
    int tries = 0;

    assert(NULL != client);
    fd = client->conn_fd;

    while( tries < 3) {
        len = recv(fd, client->next_p, client->recv_len, MSG_DONTWAIT);

        if( -1 == len ) {
            if (EAGAIN == errno || EWOULDBLOCK == errno) {
                // no data read because there is no data in the socket
                // should not happen as we are awaked by epoll, but we won't fail it
                return 0;
            } else if ( EINTR == errno) {
                // no data read because of interruption. Try at most 3 times. 
                tries++;
            } else {
                // log error and return -1
                xliw_agent_log(XLIW_LOG_ERR, "Failed to read %d bytes data from fd:%d",
                                              client->recv_len, fd);
                return -1;
            }
        } else if( len > 0) {
            client->next_p += len;
            client->recv_len -= len;

            if( 0 == client->recv_len) {
                // Full message has been read
                if( -1 == xliw_dispatch_msg(client, client->xliw))
                    return -1;

                // set to read next message
                if (posix_memalign((void **)&client->xliw, XLIW_ALIGN, MEM_ALLOC_SIZE) || 
                        0 == client->xliw) {
                    xliw_agent_log(XLIW_LOG_ERR, "No enough memeory on %s size %d", 
                            "xliw_t", MEM_ALLOC_SIZE); 
                    return -1;
                }
                client->next_p = (char*)client->xliw;
                client->recv_len = MEM_ALLOC_SIZE; 

                // To treat the clients equally, each iteration, at most 1 msg is read
                // per client connection if read_all is false
                if( !read_all)
                    return 0;
            }
        } else {
            return -1;
        }
    }
    return 0;
}

//worker threads
static void* xliw_worker_thread_start( void *arg)
{
    xliw_worker_t   *worker = (xliw_worker_t*)arg;
	const int BATCHSZ_MAX = 4;
    xliw_mq_entry_t *entry[BATCHSZ_MAX];
    xliw_client_t   *client[BATCHSZ_MAX]; 
    xliw_t          *xliw[BATCHSZ_MAX + 1];		// plus one for trailing NULL.
	int batchsz = 0;

    xliw_agent_log(XLIW_LOG_INFO, "A worker thread starts ...");

    for(;;) {
        // get request
        sem_wait(&worker->msgs_waited);
        pthread_mutex_lock(&(worker->mutex));
		for (batchsz = 0; batchsz < BATCHSZ_MAX; batchsz++) { 
			entry[batchsz] = TAILQ_FIRST(&(worker->qhead));
			if( entry[batchsz] != NULL) {
				TAILQ_REMOVE(&(worker->qhead), entry[batchsz], entries);
				worker->msg_count--;
			} else {
				break;
			}
		}
        pthread_mutex_unlock(&(worker->mutex));


		for (int i = 0; i < batchsz; i++) {
			if (entry[i] && entry[i]->xliw == 0) {
				for (int j = 0; j < i; j++) {
					if (entry[j] &&  entry[i]->client == entry[j]->client) {
						xliw_free(entry[j]->xliw);
						entry[j] = 0;
					}
				}
				xliw_free_client(entry[i]->client);
				entry[i] = 0;
			}
		}

		int batchsz2 = 0;
		for (int i = 0; i < batchsz; i++) {
			if (entry[i]) {
				entry[batchsz2] = entry[i];
				batchsz2++;
			}
		}
		batchsz = batchsz2;

		if( batchsz == 0) {
			continue;
		}

		for (int i = 0; i < batchsz; i++) {
			xliw[i] = entry[i]->xliw;
			client[i] = entry[i]->client;
		}
		// NULL terminal xliw array.
		xliw[batchsz] = NULL;

        // process the xliw request, in batch.
        int exec_us = 0;
#if 0
        int result = xliw_execute(xliw, &exec_us);
#else
        xliw_execute_fpga(worker->worker_nth, xliw, &exec_us);
#endif

        xliw_agent_log(XLIW_LOG_DEBUG, "Worker %d Processed %d xliw request from clients, in %d us",
					worker->worker_nth, batchsz, exec_us);

        // send result back to client.
		for (int i = 0; i < batchsz; i++) {
			int len = MEM_ALLOC_SIZE; 
			char *buf = (char*)xliw[i];
			while(len > 0) {
				int sent_len = send(client[i]->conn_fd, buf, len, MSG_NOSIGNAL);
				if (-1 == sent_len) {
					if ( EINTR == errno) {
						continue;
					}
					break;
				} else if (0 == sent_len) {
					// connection is teared down by peer
					break;
				}
				len -= sent_len;
				buf += sent_len;
			}

			// free resources
			xliw_free(xliw[i]);
			free(entry[i]);
		}
	}

    // should never be reached
#if 1
	xliw_deinit_fpga();
#endif
    return NULL;
}

void xliw_agent_cleanup()
{
    if(s_logger.fp)
        fclose(s_logger.fp);
}

int xliw_agent_start(char *logfile_path,
                      char *unix_path,
                      int  worker_num,
                      int  log_level)
{
    int            err = 0;

    //init logger
    char filename[PATH_MAX];
    struct timeval tv;
    struct tm *tm;

    gettimeofday(&tv, NULL);
    tm = gmtime(&tv.tv_sec);
    sprintf(filename, "%s/xliwagent-%04d-%02d-%02d_%02d%02d%02d.log", 
                     logfile_path, tm->tm_year + 1900, tm->tm_mon+1,
                     tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);
    xliw_agent_log_init(filename, log_level);

    //start worker threads
    if(worker_num > MAX_WORKER_NUM)
        s_worker_num = MAX_WORKER_NUM;
    else if( worker_num > 0)
        s_worker_num = worker_num;

    for(int i=0; i<s_worker_num; i++) {
        xliw_worker_t *worker = &s_workers[i];
        memset(worker, 0, sizeof(xliw_worker_t));

        TAILQ_INIT(&(worker->qhead));
		worker->worker_nth = i;
        worker->msg_count = 0;
        if(pthread_mutex_init(&(worker->mutex), NULL) != 0) {
            xliw_agent_cleanup();
            return -1;
        }

        if( sem_init(&(worker->msgs_waited), 0, 0) != 0 ){
            pthread_mutex_destroy(&(worker->mutex));
            xliw_agent_cleanup();
            return -1;
        }
        
        // create the worker thread
        if ((err = pthread_create( &worker->thread,
                        0,
                        xliw_worker_thread_start,
                        worker)) != 0 ) {
            xliw_agent_cleanup();
            return -1;
        }

    }

    err = xliw_agent_read(unix_path);
    xliw_agent_cleanup();

    return err;
}
