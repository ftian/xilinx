eXtra Long Instruction Word 
===========================

Hash
----

This is the C reference impl of XLIW for Deepgreen Database. On intel core i5, we 
packed ~230K 0-127 bytes strings into 16M buffer.

Packing takes 6 to 10 ms.
SHA1 hash takes 60 - 70 ms.
SHA224 time swing from 100ms to 200ms.
SORT takes extra ~70 ms.

GIS
---

pointpoly.csv and polypoly.csv are table dumps.   One can load the data into a 
postgres (postgis enabled) database.
```
create table pointpoly(
    point public.geometry(Point,4326),
    poly public.geometry(MultiPolygon,4326)
);
copy pointpoly './pointpoly.csv' delimiter ',';
create table polypoly(
    poly1 public.geometry(MultiPolygon,4326),
    poly2 public.geometry(MultiPolygon,4326)
);
copy pointpoly './polypoly.csv' delimiter ',';
```

ptply.txt and plyply.txt are the points (x, y) of pointpoly and polypoly, with a 
third column saying if they intersects (t/f).

xliw_gis_test just load the txt file, construct xliw, then call xliw_execute.   Note 
that the execute will decode the xliw, but fake result (all false).  Correct result 
can be loaded from txt file.
