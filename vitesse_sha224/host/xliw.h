#ifndef _XLIW_H_
#define _XLIW_H_

#include <stdint.h>
#include <sys/time.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * XLIW: eXtra Long Istruction Word.
 *
 * Each xliw consists of a header, xliw_t, then followed by an
 * array of xliw_region.
 *
 * Inside each region are a list of entries.  An entry is always
 * 4 bytes aligned, with a 4 bytes length header plus binary data,
 * then 0-filled to 4 bytes alignment.   A region is simply a
 * sequence of such entries.
 *
 * For HASH_RESULT, a xliw has 0 region.   Just an array of hash_results.
 */

typedef enum XLIW_INSTRUCTION {
  XLIW_I_SHA224,
  XLIW_I_SHA224_SORT,
  XLIW_I_HASH_RESULT,
  XLIW_I_HASH_RESULT_ERR,
} XLIW_INSTRUCTION;

/*
 * region is a sequence of data payload, packed as part of xliw.
 * region meta data is 16 bytes.
 */
typedef struct xliw_region_s {
  int32_t offset; /* region start, as offset into xliw */
  int32_t length; /* length of the region */
  int32_t idx;    /* starting idx. */
  int32_t cnt;    /* number of entries in this region. */
} xliw_region_t;

/*
 * computed hash.  32 bytes.
 */
typedef struct xliw_hash_s {
  int8_t sha[28]; /* SHA224, or SHA1 (20 bytes + 8 zero) */
  int32_t idx;
} xliw_hash_t;

/*
 * XLIW header, 32 bytes.
 */
typedef struct xliw_s {
  int32_t len;        /* total length of xliw */
  int32_t xliw;       /* the instruction, see XLIW_T */
  int32_t hcnt;       /* count of entries, total. */
  int32_t rcnt;       /* count of regions */
  uint64_t chksum[2]; /* chksum, or scratch if not sealed. */
} xliw_t;

static inline xliw_region_t* xliw_region(xliw_t* xliw, int32_t nth) {
  xliw_region_t* r0 = (xliw_region_t*)&xliw[1];
  return r0 + nth;
}

static inline int8_t* xliw_region_data(xliw_t* xliw, int32_t nth) {
  xliw_region_t* r = xliw_region(xliw, nth);
  int8_t* p = (int8_t*)xliw;
  return p + r->offset;
}

static inline xliw_hash_t* xliw_hash(xliw_t* xliw, int32_t nth) {
  xliw_hash_t* h0 = (xliw_hash_t*)&xliw[1];
  return h0 + nth;
}

/*
 * Execute an XLIW.
 *
 * Execute XLIW, result is putback into same xliw buffer.
 * For HASH or HASH_SORT, each entry will result in one xliw_hash_t.
 * The input xliw must be long enough to hold the computed results.
 *
 * Return >=0 on sucess, where xliw will contain the result of execution.
 * Return <0 on failure.  xliw data will be garbage.
 */
int xliw_execute_cpu(xliw_t* xliw, int* exec_us);

/*
 * code for building xliw
 */
#define XLIW_SZ (16 * 1024 * 1024)
#define XLIW_NREGION (128)
#define XLIW_ALIGN (4096)

/*
 * Allocating xliw_t.
 */
xliw_t* xliw_alloc(int32_t xliw_ins);
void xliw_free(xliw_t* xliw);

/*
 * Pack payload data into xliw.
 * return
 *	>=0 if packed OK.
 * -1 if not enough space.
 */
int xliw_pack(xliw_t* xliw, char* payload, int32_t sz);

/*
 * Seal.
 */
void xliw_seal(xliw_t* xliw);

static inline int tvdiff(struct timeval* tv0, struct timeval* tv1) {
	return (tv1->tv_sec - tv0->tv_sec) * 1000000 + (tv1->tv_usec - tv0->tv_usec);
}

/* 
 * Clear
 */
void xliw_clear(xliw_t *xliw, int32_t ins);


#define XLIW_HAVE_FPGA
#ifdef XLIW_HAVE_FPGA
int xliw_init_fpga(const void *p, int ndev);
void xliw_deinit_fpga();
int xliw_execute_fpga(int n, xliw_t** xliw, int* exec_us);
#endif

#ifdef __cplusplus
}
#endif

#endif /* _XLIW_H_ */
