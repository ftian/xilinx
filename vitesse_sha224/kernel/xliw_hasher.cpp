/*****************************************************************************
 *
 *     Author: Xilinx, Inc.
 *
 *     This text contains proprietary, confidential information of
 *     Xilinx, Inc. , is distributed by under license from Xilinx,
 *     Inc., and may be used, copied and/or disclosed only pursuant to
 *     the terms of a valid license agreement with Xilinx, Inc.
 *
 *     XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"
 *     AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND
 *     SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,
 *     OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,
 *     APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION
 *     THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,
 *     AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE
 *     FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY
 *     WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE
 *     IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR
 *     REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF
 *     INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *     FOR A PARTICULAR PURPOSE.
 *
 *     Xilinx products are not intended for use in life support appliances,
 *     devices, or systems. Use in such applications is expressly prohibited.
 *
 *     (c) Copyright 2018 Xilinx Inc.
 *     All rights reserved.
 *
 *****************************************************************************/

#include <ap_int.h>

#include "xliw_hasher.h"
#include "sha224_cluster.h"

// used modules
#include <hls_stream.h>

#include "hls_alg/types.h"
#include "hls_alg/utils.h"
#include "hls_alg/debug.h"

static inline ap_uint<8 * INPUT_SZ> x_cast(INPUT_T x) {
  ap_uint<8 * INPUT_SZ> y;
  for (int i = 0; i < INPUT_SZ; ++i) {
#pragma HLS unroll
    y.range(7 + 8 * i, 8 * i) = x.byte[i];
  }
  return y;
}

template <int N>
struct CharArray {
  char byte[N];
};

template <typename T, int W>
static inline T x_cast(ap_uint<W> x) {
  HLS_ALG_STATIC_ASSERT(sizeof(T) * 8 == W, "Cast type of different size.");
  const int sz = W >> 3;
  CharArray<sz> c;
L_X_CAST_T:
  for (int j = 0; j < sz; ++j) {
#pragma HLS unroll
    c.byte[j] = ap_uint<8>(x.range(7 + 8 * j, 8 * j)).to_char();
  }
  union {
    CharArray<sz> as_c;
    T as_T;
  } t;
  t.as_c = c;
  T y = t.as_T;
  return y;
}

// ------------------------ work with region stream ---------------------------

/// @brief read the header to extract region
static void region_reader(INPUT_T* xliw_addr,
                          hls::stream<xliw_region_t>& region_strm,
                          hls::stream<int32_t>& region_nword_strm,
                          hls::stream<ap_uint<8 * INPUT_SZ> >& word_strm) {

  const int xliw_t_sz = sizeof(xliw_t);
  HLS_ALG_STATIC_ASSERT(xliw_t_sz == 32, "xliw_t size is not 32!");

  const int xliw_region_t_sz = sizeof(xliw_region_t);
  HLS_ALG_STATIC_ASSERT(xliw_region_t_sz == 16, "xliw_region_t size is not 16!");

  // read xliw
  ap_uint<8 * INPUT_SZ> uaxi = x_cast(xliw_addr[0]);

  // with 2 more, as INPUT_SZ access will over-read.
  xliw_region_t regions[XLIW_NREGION + 2]; // remove, put directly into stream.

  xliw_t xliw =
      x_cast<xliw_t>(ap_uint<8 * xliw_t_sz>(uaxi.range(8 * xliw_t_sz - 1, 0)));

  if (xliw.rcnt > 0) {
    regions[0] = x_cast<xliw_region_t>(ap_uint<8 * xliw_region_t_sz>(
        uaxi.range(8 * xliw_region_t_sz + 8 * xliw_t_sz - 1, 8 * xliw_t_sz)));
  }
  if (xliw.rcnt > 1) {
    regions[1] = x_cast<xliw_region_t>(ap_uint<8 * xliw_region_t_sz>(
        uaxi.range(8 * xliw_region_t_sz * 2 + 8 * xliw_t_sz - 1,
                   8 * xliw_region_t_sz + 8 * xliw_t_sz)));
  }
  // HLS_ALG_PRINT("DEBUG: read region 0 to 1\n");

  // read from ddr to buffer
  for (int i = 2; i < ((xliw.rcnt - 2 + 3) & (-1 ^ 3)); i += 4) {
#pragma HLS pipeline II = 2
    INPUT_T a = xliw_addr[1 + (i >> 2)];
    uaxi = x_cast(a);
    regions[i] = x_cast<xliw_region_t>(
        ap_uint<8 * xliw_region_t_sz>(uaxi.range(8 * xliw_region_t_sz - 1, 0)));
    regions[i + 1] = x_cast<xliw_region_t>(ap_uint<8 * xliw_region_t_sz>(
        uaxi.range(8 * xliw_region_t_sz * 2 - 1, 8 * xliw_region_t_sz)));
    regions[i + 2] = x_cast<xliw_region_t>(ap_uint<8 * xliw_region_t_sz>(
        uaxi.range(8 * xliw_region_t_sz * 3 - 1, 8 * xliw_region_t_sz * 2)));
    regions[i + 3] = x_cast<xliw_region_t>(ap_uint<8 * xliw_region_t_sz>(
        uaxi.range(8 * xliw_region_t_sz * 4 - 1, 8 * xliw_region_t_sz * 3)));
    // HLS_ALG_PRINT("DEBUG: read region %d to %d\n", i, i + 3);
  }

  int32_t result_cnt = 0;

  for (int j = 0; j < xliw.rcnt; ++j) {
    xliw_region_t region = regions[j];

    // XXX assume xliw_addr is aligned to INPUT_SZ
    int32_t a0 = region.offset;
    int32_t a1 = region.offset + region.length;

    a0 &= (-1L ^ (INPUT_SZ - 1));
    a1 &= (-1L ^ (INPUT_SZ - 1));

    a0 /= INPUT_SZ;
    a1 /= INPUT_SZ;

    INPUT_T* r_begin_addr = xliw_addr + a0;
    INPUT_T* r_end_addr = xliw_addr + a1;

    int32_t nword = a1 - a0 + 1;

    region_strm.write(region);
    region_nword_strm.write(nword);

  L_DDR_TO_WORD_P:
    for (int i = 0; i < nword; ++i) {
#pragma HLS pipeline
      INPUT_T a = r_begin_addr[i];
      ap_uint<8 * INPUT_SZ> u = x_cast(a);
      word_strm.write(u);
    }

#if 0
    HLS_ALG_PRINT("DEBUG: region %d requested %d word for %d entries (%p-%p)\n",
                j, nword, region.cnt, r_begin_addr, r_end_addr);
#endif

    result_cnt += region.cnt;
  }
  region_nword_strm.write(-1);

  HLS_ALG_PRINT("DEBUG: result_cnt = %d, xliw.hcnt = %d\n", result_cnt, xliw.hcnt);
  HLS_ALG_ASSERT(result_cnt == xliw.hcnt && "result number mismatch");
}

/// @brief extract variable length tuple from fixed size input word
static void word_to_tuple(hls::stream<xliw_region_t>& region_strm,
                          hls::stream<int32_t>& region_nword_strm,
                          hls::stream<ap_uint<8 * INPUT_SZ> >& word_strm,
                          // output
                          hls::stream<ap_uint<64> >& msg_strm,
                          hls::stream<uint64_t>& len_strm,
                          hls::stream<bool>& end_len_strm,
                          hls::stream<int32_t>& idx_strm) {

  int32_t nword = region_nword_strm.read();
  int rid = 0;
  int tcnt = 0;
  while (nword >= 0) { // XXX -1 to exit

    xliw_region_t region = region_strm.read();
    int32_t cnt = region.cnt;
    int32_t offset = region.offset;
    int32_t idx = region.idx;
    HLS_ALG_PRINT("DEBUG: reading %d word for %d tuples in region %d\n",
                nword, cnt, rid);
    //
    if (cnt != 0) {
      enum { BUF_SZ = INPUT_SZ + 4 };
      ap_uint<8 * BUF_SZ> buf = word_strm.read();

      /// number of ddr read.
      int nread = 1;

      /// number of byte in buf.
      // XXX int nbuf;
      ap_uint<8> nbuf;

      // XXX this assume xliw is aligned to INPUT_SZ.
      if (offset == 0) {
        nbuf = INPUT_SZ;
      } else {
        // XXX buf >>= 8 * (offset % INPUT_SZ);
        // XXX nbuf = INPUT_SZ - (offset % INPUT_SZ);
        HLS_ALG_ASSERT(offset >= 0 && "negtive offset is invalid!");
        ap_uint<9> a;
        a.range(8, 3) = offset;
        a.range(2, 0) = 0;
        buf >>= a;

        nbuf = INPUT_SZ - ap_uint<6>(offset);
      }
      HLS_ALG_ASSERT(nbuf >= 0 && "offset handling has a bug?");

      /// number of tuple.
      int32_t ntuple = 0;

      /// message tuple length aligned to in 32b
      // XXX int32_t mlen = 0;
      ap_uint<8> mlen = 0;

    L_WORD_TO_TUPLE_P:
      for(; (ntuple < cnt) || (nread < nword); ) {
        if (ntuple < cnt) {
          if (mlen == 0) {
            // send 32b length
            int32_t len = buf.range(31, 0).to_long();
            len_strm.write((uint64_t)len);
            end_len_strm.write(false);
            idx_strm.write((int32_t)(idx + ntuple));
            mlen = (ap_uint<8>(len) + ap_uint<8>(3)) & ap_uint<8>(-1L ^ 3);
            if (mlen == 0) {
              ++ntuple;
              ++tcnt;
            }
            //printf("\n%d.%d: len=%d, mlen=%d, nread=%d ", 
            //       rid, ntuple, (int)len, mlen.to_int() / 4, nread);
            HLS_ALG_ASSERT(len <= MAX_MSG_SZ && "invalid message length");
            // tuple length aligned to 4 Byte.
            // shift or refill
            if (nbuf != 4) {
              buf >>= 32;
              nbuf -= 4;
            } else {
              buf = word_strm.read();
              ++nread;
              HLS_ALG_ASSERT(nread <= nword && "overread");
              nbuf = INPUT_SZ;
            }
          } else {
            // try to send 64bit message word
            if (mlen == 4) {
              // only need 32bit.
              ap_uint<64> word;
              word.range(31, 0) =  buf.range(63, 0).to_ulong();
              word.range(63, 32) = 0;
              msg_strm.write(word);
              ++ntuple;
              ++tcnt;
              mlen = 0;
              // refill or shift
              if (nbuf == 4) {
                buf = word_strm.read();
                ++nread;
                HLS_ALG_ASSERT(nread <= nword && "overread");
                nbuf = INPUT_SZ;
              } else if ((nbuf == 8) && (nread < nword)) {
                buf.range(31, 0) = buf.range(63, 32);
                buf.range(8 * BUF_SZ - 1, 32) = word_strm.read();
                ++nread;
                HLS_ALG_ASSERT(nread <= nword && "overread");
                nbuf = INPUT_SZ + 4;
              } else {
                buf >>= 32;
                nbuf -= 4;
              }
            } else {
              // need 64bit.
              if (nbuf <= 4) {
                // must refill first
                buf.range(8 * BUF_SZ - 1, 32) = word_strm.read();
                ++nread;
                HLS_ALG_ASSERT(nread <= nword && "overread");
                nbuf = INPUT_SZ + 4;
              } else {
                // enough to send message 64b word
                ap_uint<64> word = buf.range(63, 0).to_uint64();
                msg_strm.write(word);
                if (mlen == 8) {
                  ++ntuple;
                  ++tcnt;
                }
                mlen -= 8;
                // printf(".");
                if (nbuf != 8) {
                  buf >>= 64;
                  nbuf -= 8;
                } else if (nread < nword) {
                  buf = word_strm.read();
                  ++nread;
                  HLS_ALG_ASSERT(nread <= nword && "overread");
                  nbuf = INPUT_SZ;
                }
              }
            }
          }
          HLS_ALG_ASSERT(nbuf > 0 && "buf empty?");
        } else { // nread < nword
          word_strm.read();
          ++nread;
          HLS_ALG_ASSERT(nread <= nword && "overread");
        }
      }
      // HLS_ALG_PRINT("DEBUG: sent %d tuples from region %d, used %d words.\n",
      //             ntuple, rid, nread);
    } else {
      // cnt = 0
      for (int nread = 0; nread < nword; ++nread) {
        word_strm.read();
      }
      // HLS_ALG_PRINT("DEBUG: sent 0 tuples from region %d, used %d words.\n",
      //             rid, nword);
    }
    //
    // next
    ++rid;
    nword = region_nword_strm.read();
  }
  HLS_ALG_PRINT("DEBUG: totally %d tuple sent from word_to_tuple\n", tcnt);
  end_len_strm.write(true);
}

/// @brief write SHA-224 result as specified by VDB.
static void sha224_emit_result(hls::stream<ap_uint<224> >& hash_strm,
                               hls::stream<bool>& end_hash_strm,
                               hls::stream<int32_t>& idx_strm,
                               //
                               hls::stream<xliw_hash_t>& result_strm,
                               hls::stream<bool>& end_result_strm) {

  bool flag = end_hash_strm.read();
  int rcnt = 0;
L_SHA224_EMIT_RES_P:
  while(!flag) {
#pragma HLS pipeline
    xliw_hash_t r;
    ap_uint<224> h = hash_strm.read();
    for (int i = 0; i < 28; ++i) {
#pragma HLS unroll
      r.sha[i] = h.range(7 + 8 * i, 8 * i);
    }
    r.idx = idx_strm.read();
    //
    result_strm.write(r);
    end_result_strm.write(false);
    ++rcnt;
    // next?
    flag = end_hash_strm.read();
  }
  end_result_strm.write(true);
  HLS_ALG_PRINT("DEBUG: totally %d result sent from sha224_emit_result\n", rcnt);
} // sha224_emit_result


template<unsigned char burst_n>
static void result_counter(hls::stream<bool>& end_result_strm,
                           hls::stream<unsigned char>& data_len_strm) {
  bool end = end_result_strm.read();
  unsigned char n = 0;
L_SHA224_RESULT_COUNTER:
  while (!end) {
    if (n == burst_n - 1) {
      n = 0;
      end = end_result_strm.read();
      data_len_strm.write(burst_n);
    } else {
      ++n;
      end = end_result_strm.read();
    }
  }
  data_len_strm.write(n);
  // need extra end notify.
  if (n > 0) {
    data_len_strm.write(0);
  }
}

template<unsigned char burst_n>
static void result_burster(
    OUTPUT_T* result_addr,
    hls::stream<xliw_hash_t>& result_strm,
    hls::stream<unsigned char>& data_len_strm) {
  unsigned char n = data_len_strm.read();
  int i = 0;
L_SHA224_RESULT_OUT:
  while (n != 0) {
L_SHA224_RESULT_BURST_P:
    for (int j = 0; j < n; ++j) {
#pragma HLS pipeline II = 1
      xliw_hash_t x = result_strm.read();
      union {
        OUTPUT_T as_o;
        xliw_hash_t as_x;
      } u;
      u.as_x = x;
      OUTPUT_T o = u.as_o;
      result_addr[i++] = o;
    }
    // next burst
    n = data_len_strm.read();
  }
  HLS_ALG_PRINT("DEBUG: totally %d result written to DDR in "
                "result_burster.\n", i);
}

/// @brief write result structure to DDR
static void result_to_ddr(OUTPUT_T* result_addr, //
                          hls::stream<xliw_hash_t>& result_strm,
                          hls::stream<bool>& end_result_strm) {
#pragma HLS dataflow

  hls::stream<unsigned char> data_len_strm("data_len_strm");
#pragma HLS STREAM variable = data_len_strm depth = 2 dim = 1

  result_counter<64>(end_result_strm, data_len_strm);
  result_burster<64>(result_addr, result_strm, data_len_strm);
}

// --------------------------- top interface -----------------------------------

extern "C" {
void KERNEL_NAME(INPUT_T xliw_addr[INPUT_DEPTH],
                 OUTPUT_T result_addr[OUTPUT_DEPTH]) {

// define interface
#ifndef HLS_TEST

#pragma HLS INTERFACE m_axi port = xliw_addr \
  bundle = gmem0_0 offset = slave \
  num_read_outstanding = 32 num_write_outstanding = 32 \
  max_read_burst_length = 64 max_write_burst_length = 64 \
  latency = 125

#pragma HLS INTERFACE m_axi port = result_addr \
  bundle = gmem0_1 offset = slave \
  num_read_outstanding = 32 num_write_outstanding = 32 \
  max_read_burst_length = 64 max_write_burst_length = 64 \
  latency = 125

#pragma HLS INTERFACE s_axilite port = xliw_addr bundle = control
#pragma HLS INTERFACE s_axilite port = result_addr bundle = control
#pragma HLS INTERFACE s_axilite port = return bundle = control

#endif // HLS_TEST

#pragma HLS data_pack variable = xliw_addr
#pragma HLS data_pack variable = result_addr

  // check assumptions.
  HLS_ALG_STATIC_ASSERT(INPUT_SZ == 64, "INPUT_SZ not supported yet!");

  // check input address

#define XSTR(a) XSTR_(a)
#define XSTR_(a) #a

  HLS_ALG_PRINT("DEBUG: " XSTR(KERNEL_NAME) ": xliw_addr = %p, result_addr = %p\n",
              xliw_addr, result_addr);

  HLS_ALG_ASSERT(((uintptr_t)xliw_addr & (INPUT_SZ - 1)) == 0 &&
               "xliw_addr not aligned!");

#pragma HLS dataflow

  hls::stream<int32_t> region_nword_strm("region_nword_strm"); // also use to terminate
#pragma HLS STREAM variable = region_nword_strm depth = 4 dim = 1

  hls::stream<xliw_region_t> region_strm("region_strm");
#pragma HLS STREAM variable = region_strm depth = 4 dim = 1

  hls::stream<ap_uint<8 * INPUT_SZ> > word_strm("word_strm"); // XXX deeper for burst.
#pragma HLS STREAM variable = word_strm depth = 32 dim = 1
  //
  hls::stream<ap_uint<64> > msg_strm("msg_strm");
#pragma HLS STREAM variable = msg_strm depth = 64 dim = 1

  hls::stream<uint64_t> len_strm("len_strm");
#pragma HLS STREAM variable = len_strm depth = 4 dim = 1

  hls::stream<bool> end_len_strm("end_len_strm");
#pragma HLS STREAM variable = end_len_strm depth = 4 dim = 1

  hls::stream<int32_t> idx_strm("idx_strm");
#pragma HLS STREAM variable = idx_strm depth = 256 dim = 1
  //
  hls::stream<ap_uint<224> > hash_strm("hash_strm");
#pragma HLS STREAM variable = hash_strm depth = 4 dim = 1

  hls::stream<bool> end_hash_strm("end_hash_strm");
#pragma HLS STREAM variable = end_hash_strm depth = 4 dim = 1
  //
  hls::stream<xliw_hash_t> result_strm("result_strm");
#pragma HLS STREAM variable = result_strm depth = 80 dim = 1

  hls::stream<bool> end_result_strm("end_result_strm");
#pragma HLS STREAM variable = end_result_strm depth = 80 dim = 1

  region_reader(xliw_addr, // ddr
                region_strm, region_nword_strm, word_strm); // out

  word_to_tuple(region_strm, region_nword_strm, word_strm, // in
                msg_strm, len_strm, end_len_strm, idx_strm); // out

#if 0
  hls_alg::sha224(msg_strm, len_strm, end_len_strm,// in
                 hash_strm, end_hash_strm); // out
#else
  sha224_cluster<12>(msg_strm, len_strm, end_len_strm,// in
                    hash_strm, end_hash_strm); // out
#endif

  sha224_emit_result(hash_strm, end_hash_strm, idx_strm, // in
                     result_strm, end_result_strm); // out

  result_to_ddr(result_addr, // ddr
                result_strm, end_result_strm); // in

}
} // extern "C"
