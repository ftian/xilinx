/*
  __VIVADO_HLS_COPYRIGHT-INFO__
 */

/**
 * @file utils.h
 * @brief This file is part of HLS algorithm library, contains utilities.
 */

#ifndef _X_HLS_ALG_UTILS_H_
#define _X_HLS_ALG_UTILS_H_

#ifndef __SYNTHESIS__
// for assert function.
#include <cassert>
#define HLS_ALG_ASSERT(b) assert((b))
#else
#define HLS_ALG_ASSERT(b) ((void)0)
#endif

#if __cplusplus >= 201103L
#define HLS_ALG_STATIC_ASSERT(b, m) static_assert((b), m)
#else
#define HLS_ALG_STATIC_ASSERT(b, m) HLS_ALG_ASSERT((b) && (m))
#endif

#define HLS_ALG_MACRO_QUOTE(s) #s
#define HLS_ALG_MACRO_STR(s) HLS_ALG_MACRO_QUOTE(s)

#endif // _X_HLS_ALG_UTILS_H_
// -*- cpp -*-
// vim: ts=8:sw=2:sts=2:ft=cpp
