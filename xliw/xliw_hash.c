#include "xliw.h"
#include <openssl/sha.h>

static int cmpfn(const void *a, const void *b)
{
	return memcmp(a, b, sizeof(xliw_hash_t));
}

/* #define XLIW_USE_SHA224 1 */
int xliw_execute_hash(xliw_t *xliw)
{
	int32_t resultsz = sizeof(xliw_t) + xliw->hcnt * sizeof(xliw_hash_t);
	xliw_t *result = (xliw_t *) aligned_alloc(32, resultsz); 
	if (!result) {
		/* OOM */
		return -1;
	}

#ifdef XLIW_USE_SHA224 
	memset(result, 0, sizeof(xliw_t));
#else
	memset(result, 0, resultsz); 
#endif

	result->len = resultsz;
	result->xliw = XLIW_I_HASH_RESULT;
	result->hcnt = xliw->hcnt;

	/* 
	 * Notice the loop, there should be no hazzard at all if we do 
	 * not use a loop, but use rcnt threads instead.
	 */
	for (int i = 0; i < xliw->rcnt; i++) {
		xliw_region_t *r = xliw_region(xliw, i);
		if (r->length == 0) {
			/* empty region */
			continue;
		}

		int8_t *p = xliw_region_data(xliw, i);
		for (int32_t next = 0; next < r->cnt; next++) {
			int32_t idx = r->idx + next;
			xliw_hash_t *h = xliw_hash(result, idx); 

			int32_t sz = *((int32_t *) p);
			p += sizeof(int32_t);
			
#ifdef XLIW_USE_SHA224 
			/* compute hash */ 
			SHA224((const unsigned char *)p, sz, (unsigned char*) h);
#else
			SHA1((const unsigned char *)p, sz, (unsigned char*) h);
#endif
			h->idx = idx;

			p += sz;
			p = exx_align_zero(p, 4);
		}
	}

	if (xliw->xliw == XLIW_I_SHA224_SORT) {
		xliw_hash_t *h = xliw_hash(result, 0); 
		qsort(h, result->hcnt, sizeof(xliw_hash_t), cmpfn);
	}

	memcpy(xliw, result, resultsz);
	xliw_free(result);
	return 0;
}

