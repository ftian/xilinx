#include <algorithm>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <vector>

#include <sys/time.h>

#include "xliw.h"
#include "xliw_hasher.h"
#include "../ext/libs/xcl2/xcl2.hpp" 

template <typename T>
T* aligned_alloc(std::size_t num)
{
	void* ptr = nullptr;
	if (posix_memalign(&ptr,4096,num*sizeof(T)))
		throw std::bad_alloc();
	return reinterpret_cast<T*>(ptr);
}

#define NUM_KERNEL 4

struct xliw_fpga_ctxt {
	const char *binpath;

	std::vector<cl::Device> devices;

	cl::Context *context; 
	cl::Program *program;
	cl::Program::Binaries xclBins;

	cl::CommandQueue *q[NUM_KERNEL]; 
	cl::Kernel *kernels[NUM_KERNEL]; 
	unsigned flags[NUM_KERNEL];
};

static xliw_fpga_ctxt g_ctxt;

int xliw_init_fpga(const void *p, int ndev)
{
	g_ctxt.binpath = (const char *) p;
	g_ctxt.devices = xcl::get_xil_devices();

	g_ctxt.context = new cl::Context(g_ctxt.devices[0]);

	g_ctxt.xclBins = xcl::import_binary_file(g_ctxt.binpath);
	g_ctxt.devices.resize(1);
	g_ctxt.program = new cl::Program(*g_ctxt.context, g_ctxt.devices, g_ctxt.xclBins);

	g_ctxt.q[0] = new cl::CommandQueue(*g_ctxt.context, g_ctxt.devices[0],
			CL_QUEUE_PROFILING_ENABLE | CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE);
	g_ctxt.q[1] = new cl::CommandQueue(*g_ctxt.context, g_ctxt.devices[0],
			CL_QUEUE_PROFILING_ENABLE | CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE);
	g_ctxt.q[2] = new cl::CommandQueue(*g_ctxt.context, g_ctxt.devices[0],
			CL_QUEUE_PROFILING_ENABLE | CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE);
	g_ctxt.q[3] = new cl::CommandQueue(*g_ctxt.context, g_ctxt.devices[0],
			CL_QUEUE_PROFILING_ENABLE | CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE);

	g_ctxt.kernels[0] = new cl::Kernel(*g_ctxt.program, "xliw_hasher_0");
	g_ctxt.kernels[1] = new cl::Kernel(*g_ctxt.program, "xliw_hasher_1");
	g_ctxt.kernels[2] = new cl::Kernel(*g_ctxt.program, "xliw_hasher_2");
	g_ctxt.kernels[3] = new cl::Kernel(*g_ctxt.program, "xliw_hasher_3");

	g_ctxt.flags[0] = XCL_MEM_DDR_BANK0;
	g_ctxt.flags[1] = XCL_MEM_DDR_BANK1;
	g_ctxt.flags[2] = XCL_MEM_DDR_BANK2;
	g_ctxt.flags[3] = XCL_MEM_DDR_BANK3;

	return 0;
}

void xliw_deinit_fpga()
{
	for (int i = 0; i < NUM_KERNEL; i++) {
		delete g_ctxt.kernels[i];
		delete g_ctxt.q[i]; 
	}
	delete g_ctxt.program;
	delete g_ctxt.context;
}

int xliw_execute_fpga(int nth, xliw_t** xliw, int* exec_us) {

	struct timeval tv0, tv1;
	if (exec_us) { 
		gettimeofday(&tv0, 0);
		*exec_us = 0;
	}

	std::vector<xliw_t *> xliws;

	int num_rep = 0;
	while (xliw[num_rep]) {
		xliws.push_back(xliw[num_rep]);
		num_rep++;
	}

	std::vector<void*> xbuffer(num_rep);
	std::vector<void*> hbuffer(num_rep);

	// TODO modify here when change number of kernel/kernel name
	std::vector<cl_mem_ext_ptr_t> mext_i(num_rep);
	std::vector<cl_mem_ext_ptr_t> mext_o(num_rep);
	std::vector<cl::Memory> buffer_in(num_rep);
	std::vector<cl::Memory> buffer_out(num_rep);
	std::vector<std::vector<cl::Event> > write_events(num_rep);
	std::vector<std::vector<cl::Event> > kernel_events(num_rep);
	std::vector<std::vector<cl::Event> > read_events(num_rep);

	for (int i = 0; i < num_rep; ++i) {
#if 0
		// TODO: can save this memcpy.  
		xbuffer[i] = aligned_alloc<INPUT_T>(INPUT_DEPTH);
		memcpy(xbuffer[i], xliws[i], XLIW_SZ);
#else
		xbuffer[i] = xliws[i];
#endif

		hbuffer[i] = aligned_alloc<OUTPUT_T>(OUTPUT_DEPTH);
		// XXX: work-around ECC read-before-write error
		memset(hbuffer[i], 0, OUTPUT_SZ * OUTPUT_DEPTH);

		mext_i[i].obj = xbuffer[i];
		mext_i[i].param = 0;
		mext_o[i].obj = hbuffer[i];
		mext_o[i].param = 0;

		// use flags from context.
		mext_i[i].flags = g_ctxt.flags[nth]; 
		mext_o[i].flags = g_ctxt.flags[nth]; 

		write_events[i].resize(1);
		kernel_events[i].resize(1);
		read_events[i].resize(1);
	}

	for (int i = 0; i < num_rep; ++i) {
		buffer_in[i] = cl::Buffer(
				*g_ctxt.context, CL_MEM_READ_ONLY | CL_MEM_EXT_PTR_XILINX | CL_MEM_USE_HOST_PTR,
				(size_t)(INPUT_SZ * INPUT_DEPTH), &mext_i[i]);
		// XXX move zero from host to prevent ECC error.
		buffer_out[i] = cl::Buffer(
				*g_ctxt.context,
				CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX | CL_MEM_USE_HOST_PTR,
				(size_t)(OUTPUT_SZ * OUTPUT_DEPTH), &mext_o[i]);
	}

	// XXX: Is this finish necessary?
	// g_ctxt.q[nth]->finish();
	// printf("Kernel and buffer ready to work\n");

	/* Timing for 8 input

	   No overlap:

	   W0-.             W2-.              W4-.              W6-.
	   '-K0---.     /   '-K2----.     /   '-K4----.     /   '-K6----.
	   '--R0-            '--R2-            '--R4-            '--R6

	   W1-.             W3-.              W5-.              W7-.
	   '-K1---.     /   '-K3----.     /   '-K5----.     /   '-K7----.
	   '--R1-            '--R3-            '--R5-            '--R7

	   With overlap:

	   W0-. W2----.     W4-.     W6-.
	   '-K0--. '-K2-/-. '-K4-/-. '-K6---.
	   '---R0-  '---R2-  '---R4   '--R6

	   W1-. W3----.     W5-.     W7-.
	   '-K1--. '-K3-/-. '-K5-/-. '-K7---.
	   '---R1-  ' --R3-  '---R5   '- R7

*/

	for (int i = 0; i < num_rep; ++i) {
		// Copy host mapped buffer to FPGA DDR
		// XXX: zero'ed buffer_out is also mirrored to device, to
		// work-around ECC error.
		std::vector<cl::Memory> bv1{buffer_in[i], buffer_out[i]};
		// XXX: we let first 4 write to free run and later ones to wait
		// for event with distance 4.
		if (i > 3) {
			g_ctxt.q[nth]->enqueueMigrateMemObjects(bv1, 0 /* 0 means from host*/,
					&read_events[i - 4], &write_events[i][0]);
		} else {
			g_ctxt.q[nth]->enqueueMigrateMemObjects(bv1, 0 /* 0 means from host*/, nullptr,
					&write_events[i][0]);
		}

		g_ctxt.kernels[nth]->setArg(0, buffer_in[i]);
		g_ctxt.kernels[nth]->setArg(1, buffer_out[i]);
		g_ctxt.q[nth]->enqueueTask(*g_ctxt.kernels[nth], 
				&write_events[i], &kernel_events[i][0]);

		// Copy result from FPGA DDR to host
		std::vector<cl::Memory> bv2{buffer_out[i]};
		g_ctxt.q[nth]->enqueueMigrateMemObjects(bv2, CL_MIGRATE_MEM_OBJECT_HOST,
				&kernel_events[i], &read_events[i][0]);
	}

	// wait all to finish.
	g_ctxt.q[nth]->flush();
	g_ctxt.q[nth]->finish();

	for (int i = 0; i < num_rep; ++i) {
		// Copy data back into xliw
		memcpy(((char*)xliws[i]) + sizeof(xliw_t), hbuffer[i],
				XLIW_SZ - sizeof(xliw_t));
		// Update header info
		xliws[i]->xliw = XLIW_I_HASH_RESULT;
		xliws[i]->len = sizeof(xliw_t) + xliws[i]->hcnt * sizeof(xliw_hash_t);

		// 
		free(hbuffer[i]);
	}

	if (exec_us) {
		gettimeofday(&tv1, 0);
		*exec_us = tvdiff(&tv0, &tv1);
	}
	return 0;
}
