#include <cstdio>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include <ap_int.h>

#include "sha224.h"

#define MAX_MSG_LEN 127
const int m_sz = 8;

void hls_db_sha224(hls::stream<ap_uint<8 * m_sz> >& msg_strm,
                   hls::stream<uint64_t>& len_strm,
                   hls::stream<bool>& end_len_strm,
                   hls::stream<ap_uint<224> >& hash_strm,
                   hls::stream<bool>& end_hash_strm) {
  hls_db::sha224(msg_strm, len_strm, end_len_strm, hash_strm, end_hash_strm);
}

struct Test {
  std::string msg;
  std::string h224;
  std::string h256;
  Test(const char* m, const char* x, const char* y) : msg(m), h224(x), h256(y) {
    if (msg.length() > MAX_MSG_LEN) {
      std::cout << "warning, message\"" << msg << "\" too long" << std::endl;
      abort();
    }
  }
};

int main(int argc, const char* argv[]) {
  std::vector<Test> tests;
  /* these values can be generated with
   *   echo -n "abc" | sha256sum,
   * where -n prevents echo to add \n after abc.
   */
  tests.push_back(
      Test("", //
           "d14a028c2a3a2bc9476102bb288234c415a2b01f828ea62ac5b3e42f",
           "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"));
  tests.push_back(
      Test("abc", "23097d223405d8228642a477bda255b32aadbce4bda0b3f7e36c9da7",
           "ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad"));
  /* 440 bits, 55 bytes, 1 block */
  tests.push_back(
      Test("abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnop",
           "7a027d88e394d289ed7a10a918b93d1f210b4741d44534ce64275ab9",
           "aa353e009edbaebfc6e494c8d847696896cb8b398e0173a4b5c1b636292d87c7"));
  /* 448 bits, 56 bytes, 2 blocks */
  tests.push_back(
      Test("abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq",
           "75388b16512776cc5dba5da1fd890150b0c6455cb4f58b1952522525",
           "248d6a61d20638b8e5c026930c3e6039a33ce45964ff2167f6ecedd419db06c1"));
  /* 64 bytes, 2 blocks */
  tests.push_back(
      Test("abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopqabcdabcd",
           "f0bc747215445f61e758d7eaa79303a9ea3fae850e5c73e05429490b",
           "3ed5297dc18ff845f53f2c5905e93b7b382fc22f35892b193258729ffd550649"));
  /* 127 bytes, 3 blocks */
  tests.push_back(
      Test("abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopqabcdabcd"
           "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopqabcdabc",
           "15620de894f71c44ce284faaa814460808142c4c2f73cbdfb16311ac",
           "13442d0df596ea050642cbb2f7d4bb499ea008705d40589c2c01d7ad60b05f88"));
  int nerror = 0;
  hls::stream<ap_uint<8 * m_sz> > msg_strm("msg_strm");
  hls::stream<uint64_t> len_strm("len_strm");
  hls::stream<bool> end_len_strm("end_len_strm");
  hls::stream<ap_uint<224> > hash_strm("hash_strm");
  hls::stream<bool> end_hash_strm("end_hash_strm");

  for (std::vector<Test>::const_iterator test = tests.begin();
       test != tests.end(); ++test) {
    // std::cout << "\nmessage: \"" << (*test).msg << "\"\n";

    // prepare input

    ap_uint<8 * m_sz> m;
    int n = 0;
    int cnt = 0;
    for (std::string::size_type i = 0; i < (*test).msg.length(); ++i) {
      if (n == 0) {
        m = 0;
      }
      m.range(7 + 8 * n, 8 * n) = (unsigned)((*test).msg[i]);
      ++n;
      if (n == m_sz) {
        msg_strm.write(m);
        ++cnt;
        n = 0;
      }
    }
    if (n != 0) {
      msg_strm.write(m);
      ++cnt;
    }
    len_strm.write((unsigned long long)((*test).msg.length()));
    end_len_strm.write(false);

    std::cout << "\nmessage: \"" << (*test).msg << "\"(" << (*test).msg.length() <<
       "bytes, " << cnt << " words)\n";
  }
  end_len_strm.write(true);

  std::cout << "input ready\n";

  // call module
  hls_db_sha224(msg_strm, len_strm, end_len_strm, hash_strm, end_hash_strm);

  // check result
  for (std::vector<Test>::const_iterator test = tests.begin();
       test != tests.end(); ++test) {
    std::cout << "\nmessage: \"" << (*test).msg << "\"(" << (*test).msg.length() << ")\n";

    ap_uint<224> h224 = hash_strm.read();
    bool x = end_hash_strm.read();

    std::ostringstream oss;
    std::string retstr;

    // check output
    oss.str("");
    oss << std::hex;
    for (int i = 0; i < 28; ++i) {
      oss << std::setw(2) << std::setfill('0')
          << (unsigned)(h224.range(8 * i + 7, 8 * i));
    }
    retstr = oss.str();
    std::cout << "return: " << retstr << "\n";
    if (retstr != (*test).h224) {
      ++nerror;
      std::cout << "golden: " << (*test).h224 << "\n";
    }
  }
  bool x = end_hash_strm.read();

  if (nerror) {
    std::cout << "\nFAIL: " << nerror << " errors found.\n";
  } else {
    std::cout << "\nPASS: no error found.\n";
  }
  return nerror;
}
