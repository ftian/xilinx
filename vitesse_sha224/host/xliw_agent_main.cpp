#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>

#include "xliw.h"
#include "xliwagent.h"

#define MAX_WORKER_NUM  16
static void print_options()
{
    printf("Valid Options\n");
    printf("    -logpath [log_file_path]\n");
    printf("            Specify the log file path for the agent.\n");
    printf("    -workers <1-%d>\n", MAX_WORKER_NUM);
    printf("            Specify the number of workers.\n");
}

int main(int argc, char* argv[])
{
    int i = 1;
    char *log_path = (char *) ".";
    int   worker_num = 4;

    while(i < argc) {
        char *option = argv[i++];

        if( 0 == strcmp(option, "-logpath")) {
            // configure filename
            log_path = argv[i++];
        } else if( 0 == strcmp(option, "-workers")) {
            worker_num = atoi(argv[i++]);
            if(worker_num <= 0 || worker_num > MAX_WORKER_NUM) {
                print_options();
                return -1;
            }
        } else {
            // invalid option
            print_options();
            return -1;
        }
    }
	
#if 1
	// init fpga device.
	xliw_init_fpga("./xclbin/xliw_hasher_xilinx_vcu1525_dynamic_5_1_hw.xclbin", 4);
#endif

    xliw_agent_start(log_path, (char *) "/tmp/xliwagent.socket", worker_num, 8);

    return 0;
}

