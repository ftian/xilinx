/* To get posix_memalign */
#define _POSIX_C_SOURCE (200112L)

#include <stdlib.h>
#include <string.h>
#include <openssl/sha.h>

#include "xliw.h"

#define XLIW_USE_SHA224 1

/*
 * These are rough estimates.   We will pack a region to approximately
 * up to these limit.   -64 is just a magic number accounting for some
 * overhead.
 */
const int32_t XLIW_REGION_X_SZ = (XLIW_SZ / XLIW_NREGION) - 64;
const int32_t XLIW_REGION_X_CNT = (XLIW_SZ / XLIW_NREGION) / 32;
const int32_t XLIW_HCNT_MAX = (XLIW_SZ / 32) - 1;

static inline uint64_t exx_align(uint64_t v, uint64_t align) {
	return (v + align - 1) & (~(align - 1));
}

static inline int exx_is_aligned(uint64_t v, uint64_t align) {
	return exx_align(v, align) == v;
}

static inline int8_t* exx_align_zero(int8_t* p, uint64_t align) {
	uintptr_t up = (uintptr_t)p;
	uint64_t zsz = 0;
	if (!exx_is_aligned(up, align)) {
		zsz = exx_align(up, align) - up;
		memset(p, 0, zsz);
	}
	return p + zsz;
}

xliw_t* xliw_alloc(int32_t xliw_ins) {
	xliw_t* xliw;
	if (posix_memalign((void **)&xliw, XLIW_ALIGN, XLIW_SZ)) { 
		return 0;
	} 
	xliw_clear(xliw, xliw_ins);
	return xliw;
}

void xliw_clear(xliw_t *xliw, int32_t xliw_ins) 
{
	uintptr_t pxliw = (uintptr_t)xliw;
	int32_t startoffset = sizeof(xliw_t) + XLIW_NREGION * sizeof(xliw_region_t);

	/* zero header.  data is not zeroed. */
	memset(xliw, 0, startoffset);
	xliw->xliw = xliw_ins;

	/*
	 * checksum is used as scratch,
	 *	chksum[0] is the write ptr,
	 *	chksum[1] is the buffer end;
	 */
	xliw->chksum[0] = pxliw + startoffset;
	xliw->chksum[1] = pxliw + XLIW_SZ;

	xliw_region_t* r0 = xliw_region(xliw, 0);
	r0->offset = startoffset;
}

void xliw_free(xliw_t* xliw) { free(xliw); }

int xliw_pack(xliw_t* xliw, char* payload, int32_t sz) {
	uint64_t needsz = exx_align(sz, 4) + 4;
	if (xliw->chksum[0] + needsz > xliw->chksum[1]) {
		return -1;
	}

	if (xliw->hcnt >= XLIW_HCNT_MAX) {
		return -1;
	}

	int32_t* p = (int32_t*)xliw->chksum[0];
	int8_t* pp = (int8_t*)&p[1];

	*p = sz;
	memcpy(pp, payload, sz);
	exx_align_zero(pp, 4);

	xliw->chksum[0] += needsz;
	xliw->hcnt += 1;

	xliw_region_t* r = xliw_region(xliw, xliw->rcnt);
	r->cnt += 1;
	r->length += needsz;

	if (r->cnt >= XLIW_REGION_X_CNT || r->length >= XLIW_REGION_X_SZ) {
		/* region is full.  switch to next. */
		if (xliw->rcnt + 1 == XLIW_NREGION) {
			/* xliw is full.  Just return */
			//printf("XXX: xliw full with %d regions. last region %d has %d entries, "
			//       "total %d entries\n", xliw->rcnt, xliw->rcnt, r->cnt, xliw->hcnt);
			return 0;
		}
		//printf("XXX: region %d packed %d entries (previous %d)\n",
		//       xliw->rcnt, r->cnt, xliw->hcnt - r->cnt);

		xliw->rcnt += 1;
		r[1].offset = r[0].offset + r[0].length;
		r[1].idx = r[0].idx + r[0].cnt;
	}
	return 1;
}

void xliw_seal(xliw_t* xliw) {
	uintptr_t pxliw = (uintptr_t)xliw;
	xliw->len = xliw->chksum[0] - pxliw;
	// FIXME rcnt was the region id, not count, add 1 to be count. 
	xliw->rcnt += 1;

#if 0
	/* 
	 * Maybe it is good idea to compute a checksum -- but too lazy.
	 * leave them as it is.
	 */
	xliw->chksum[0] = ???;
	xliw->chksum[1] = ???;
#endif
}

static inline int cmpfn(const void* a, const void* b) {
	return memcmp(a, b, sizeof(xliw_hash_t));
}

int xliw_execute_cpu(xliw_t* xliw, int* exec_us) {
	if (xliw->xliw != XLIW_I_SHA224 && xliw->xliw != XLIW_I_SHA224_SORT) {
		/* bad instruction */
		return -100;
	}

	/* 
	 * result is a short xliw, therefore, does not need to be allocated
	 * using posix_memalign, in fact, it connot unless we round it up.
	 */
	int32_t resultsz = sizeof(xliw_t) + xliw->hcnt * sizeof(xliw_hash_t); 
	xliw_t* result = (xliw_t *) malloc(resultsz);
	if (!result) {
		/* OOM */
		return -1;
	}

#ifdef XLIW_USE_SHA224
	memset(result, 0, sizeof(xliw_t));
#else
	memset(result, 0, resultsz);
#endif

	result->len = resultsz;
	result->xliw = XLIW_I_HASH_RESULT;
	result->hcnt = xliw->hcnt;

	struct timeval tv0, tv1;
	if (exec_us) {
		gettimeofday(&tv0, 0);
	}

	/*
	 * Notice the loop, there should be no hazzard at all if we do
	 * not use a loop, but use rcnt threads instead.
	 */
	//int total = 0; // XXX
	for (int i = 0; i < xliw->rcnt; i++) {
		xliw_region_t* r = xliw_region(xliw, i);
		if (r->length == 0) {
			/* empty region */
			continue;
		}

		int8_t* p = xliw_region_data(xliw, i);
		for (int32_t next = 0; next < r->cnt; next++) {
			int32_t idx = r->idx + next;
			xliw_hash_t* h = xliw_hash(result, idx);

			int32_t sz = *((int32_t*)p);
			p += sizeof(int32_t);

#ifdef XLIW_USE_SHA224
			/* compute hash */
			SHA224((const unsigned char*)p, sz, (unsigned char*)h);
#else
			SHA1((const unsigned char*)p, sz, (unsigned char*)h);
#endif
			h->idx = idx;

			p += sz;
			p = exx_align_zero(p, 4);
		}
		//printf("XXX: region %d begins at %p and has %d entries"
		//       " (previous %d)\n",
		//       i, (void*)(((uintptr_t)xliw) + r->offset), r->cnt, total);
		//total += r->cnt;
	}
	//printf("XXX: total %d entries\n", total);
	//if (total != xliw->hcnt) {
	//  printf("XXX: hcnt %d entries\n", xliw->hcnt);
	//  xliw_free(result);
	//  return -1;
	//}
	//if (xliw->xliw == XLIW_I_SHA224_SORT) {
	//  xliw_hash_t* h = xliw_hash(result, 0);
	//  qsort(h, result->hcnt, sizeof(xliw_hash_t), cmpfn);
	//}

	// FIXME if the average message size is smaller than that of xliw_hash_t,
	// overflow will happen here.
	memcpy(xliw, result, resultsz);
	xliw_free(result);

	if (exec_us) {
		gettimeofday(&tv1, 0);
		*exec_us = tvdiff(&tv0, &tv1);
	}

	return 0;
}

