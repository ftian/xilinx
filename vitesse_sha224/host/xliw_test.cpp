#include <algorithm>
#include <cstring>
#include <string>
#include <vector>

#define EXX_THREAD

#ifdef EXX_THREAD
#include <pthread.h>
#endif

#include "xliw.h" 

class ArgParser {
 public:
  ArgParser(int& argc, char** argv) {
    for (int i = 1; i < argc; ++i) mTokens.push_back(std::string(argv[i]));
  }
  bool getCmdOption(const std::string option, std::string& value) const {
    std::vector<std::string>::const_iterator itr;
    itr = std::find(this->mTokens.begin(), this->mTokens.end(), option);
    if (itr != this->mTokens.end() && ++itr != this->mTokens.end()) {
      value = *itr;
      return true;
    }
    return false;
  }

 private:
  std::vector<std::string> mTokens;
};

struct th_arg_t {
	bool run_fpga;
	int num_rep;
	int max_cnt;
	bool do_check; 
} tharg;

static void fail(const char *reason)
{
	fprintf(stderr, "FAILED: reason %s\n", reason);
	exit(100);
}

static void *thfunc(void *arg);

int main(int argc, char** argv) {
  // cmd arg parser.
  ArgParser parser(argc, argv);

  // default -num to 100
  std::string num_str;
  tharg.num_rep = 100;
  if (parser.getCmdOption("-num", num_str)) {
    tharg.num_rep = std::stoi(num_str, nullptr, 10);
  }

  std::string mode;
  std::string xclbinPath; // eg. xliw_hasher_VCU1525_hw.xclbin

  tharg.run_fpga = false;
  tharg.do_check = false;
  if (parser.getCmdOption("-mode", mode) && mode == "fpga") {
    tharg.run_fpga = true;
	tharg.do_check = true;

    if (!parser.getCmdOption("-xclbin", xclbinPath)) {
      printf("ERROR: xclbin path is not set!\n");
      return 1;
    }

    std::string check_arg;
    if (parser.getCmdOption("-check", check_arg) && check_arg == "false") {
      tharg.do_check = false; 
    }
  }

  // max number of entries in an XLIW. no limit by default.
  tharg.max_cnt = ~(1 << 31);
  std::string cnt_str;
  if (parser.getCmdOption("-cnt", cnt_str)) {
    int cnt = std::stoi(cnt_str, nullptr, 10);
    tharg.max_cnt = (cnt < tharg.max_cnt) ? cnt : tharg.max_cnt;
  }

  if (tharg.run_fpga) {
	  // init
	  xliw_init_fpga(xclbinPath.c_str(), 2); 
  }

#ifdef EXX_THREAD
  // we always run two threads
  pthread_t th[2];
  int err = 0;
  
  err = pthread_create(&th[0], NULL, thfunc, (void *) 0);
  if (err) fail("Cannot create thread 0");
  err = pthread_create(&th[1], NULL, thfunc, (void *) 1);
  if (err) fail("Cannot create thread 1");

  err = pthread_join(th[0], NULL);
  if (err) fail("Cannot join thread 0");
  err = pthread_join(th[1], NULL);
  if (err) fail("Cannot join thread 1");
#else
  thfunc((void *) 0);
#endif

  if (tharg.run_fpga) {
	  xliw_deinit_fpga();
  }
}

void *thfunc(void *arg) 
{
	int nth = (int) (intptr_t) arg;

	// FIXME XLIW_I_SHA224_SORT is not supported yet.
	int instruction = XLIW_I_SHA224;

	char data[128];
	for (int i = 0; i < 128; i++) {
		data[i] = i;
	}

	struct timeval tv0, tv1;
	int err = 0;

	int pack_us = 0;
	int exec_us = 0;
	int total_cnt = 0;

	std::vector<xliw_t*> cpu_xliws(tharg.num_rep);

	if (!tharg.run_fpga || tharg.do_check) {
		for (int i = 0; i < tharg.num_rep; i++) {
			xliw_t* xliw = xliw_alloc(instruction);
			cpu_xliws[i] = xliw;
			gettimeofday(&tv0, 0);
			// FIXME was >=0, continue add even full?
			int cnt;
			for (cnt = 0; cnt < tharg.max_cnt 
					&& xliw_pack(xliw, data, cnt % 128) > 0; cnt++) 
				;
			xliw_seal(xliw);
			gettimeofday(&tv1, 0);
			pack_us += tvdiff(&tv0, &tv1);
			total_cnt += xliw->hcnt;
		}

		printf("Thread %d CPU Timing: %d us.  Building %d XLIW: packed %d datum.\n",
				nth, pack_us, tharg.num_rep, total_cnt);

		// repeat the experiment for num_rep times on CPU
		for (int i = 0; i < tharg.num_rep; i++) {
			xliw_t* xliw = cpu_xliws[i];
			int usec = 0;
			err = xliw_execute_cpu(xliw, &usec);
			if (err < 0) fail("XLIW execute on CPU failed.");
			exec_us += usec;
		}

		printf("Thread %d CPU Timing: %d us.  Executing %d XLIW: packed %d datum.\n",
				nth, exec_us, tharg.num_rep, total_cnt);
	}

	if (!tharg.run_fpga) return 0;

	pack_us = 0;
	exec_us = 0;
	total_cnt = 0;

	std::vector<xliw_t*> fpga_xliws(tharg.num_rep);

	for (int i = 0; i < tharg.num_rep; i++) {
		xliw_t* xliw = xliw_alloc(instruction);
		fpga_xliws[i] = xliw;
		gettimeofday(&tv0, 0);
		int cnt;
		// FIXME was >=0, continue add even full?
		for (cnt = 0; cnt < tharg.max_cnt && xliw_pack(xliw, data, cnt % 128) > 0; cnt++) 
			;
		xliw_seal(xliw);
		gettimeofday(&tv1, 0);
		pack_us += tvdiff(&tv0, &tv1);
		total_cnt += xliw->hcnt;
	}

	printf("Thread %d CPU Timing: %d us.  Building %d XLIW: packed %d datum.\n",
			nth, pack_us, tharg.num_rep, total_cnt);

	const int batchsz = 1;
	for (int i = 0; i < tharg.num_rep; i+=batchsz) {
		// printf("Thread %d Processing %d-th xliw.\n", nth, i);
		int usec = 0;
		xliw_t *ptrs[batchsz];
		int j = 0;
		for (; j < batchsz; j++) {
			if (i + j == tharg.num_rep) {
				break;
			}
			ptrs[j] = fpga_xliws[i + j];
		}
		err = xliw_execute_fpga(nth, ptrs, &usec);
		if (err < 0) fail("xliw execute on fpga failed."); 
		exec_us += usec;
	}

	printf("Thread %d FPGA Timing: %d us.  Executing %d XLIW: packed %d datum.\n",
			nth, exec_us, tharg.num_rep, total_cnt);

	// optionally check and then release data
	for (int i = 0; i < tharg.num_rep; i++) {
		xliw_t* cx = cpu_xliws[i];
		xliw_t* fx = fpga_xliws[i];

		if (tharg.do_check) {
			bool correct = true;
			if (cx->hcnt != fx->hcnt) {
				printf("Result %d: CPU count of entries %d, FPGA count of entries %d, "
						"mismatch!\n", i, cx->hcnt, fx->hcnt);
				correct = false;
			} else {
				for (int j = 0; j < cx->hcnt; ++j) {
					xliw_hash_t* ch = xliw_hash(cx, j);
					xliw_hash_t* fh = xliw_hash(fx, j);
					if (memcmp(ch, fh, sizeof(xliw_hash_t))) {
						printf("Result %d: hash %d mismatch!\nCPU:  ", i, j);
						for (int k = 0; k < 28; ++k) {
							printf("%02x", (unsigned char)ch->sha[k]);
						}
						printf(", %d\nFPGA: ", ch->idx);
						for (int k = 0; k < 28; ++k) {
							printf("%02x", (unsigned char)fh->sha[k]);
						}
						printf(", %d\n", fh->idx);
						correct = false;
						break;
					}
				}
			}
			if (!correct) fail("Result checked FAILED."); 
		}

		xliw_free(cx);
		xliw_free(fx);
	}

	return 0;
}
