# Architecture Overview

![arch](doc/v5.png?raw=true "multi-PU cluster on single DDR")

2 kernels working with DDR Bank 0 and Bank 3 at 219MHz on NIMBIX(VCU1525):

  * CPU Timing: 8224218 us.  Executing 100 XLIW: packed 24313200 datum.
  * FPGA Timing: 738097 us.  Executing 100 XLIW: packed 24313200 datum.

over 11X speedup.

# env.sh

Source this to setup 2017.4 SDx environment.

# Makefile

## Mostly-used targets

  * run\_sw\_emu: software emulation.

  * run\_hw\_emu: hardware emulation.

  * run\_hw: execute on board.

## Variable that changes the test:

  * MINI\_TEST: reduce each xliw to this number of entries.

  * NUM\_RUN: pass `-num NUM_RUN` as argument, to set number of xliw to process.

## Example

```
make MINI_TEST=3000 NUM_RUN=3 run_hw_emu
```
This will compile and hardware-simulate 3 xliw inputs, each with 3000 entries.

# hls folder

HLS test reusing kernel and host code. Set `HLS_TEST` macro, which disables OpenCL and directly call kernel top.

# hls\_224 folder

HLS test on `hls_db::sha224` computation part.

# hls\_224cluster folder

HLS test on `sha224_cluster` part, which includes multiple `hls_db::sha224` cores. See README.md for preliminary profile results.

# host folder

xliw\_test.cpp: host main, based on Vitesse code.

xliw.h: Vitesse code.

xliw.c: Vitesse code.

# kernel folder

xliw\_hasher.cpp: kernel code.

sha224.h: SHA-224 core with purely stream interface.

sha224\_cluster.h: a cluster of SHA-224 cores with identical stream interface as single core but higher processing speed.
