#ifndef _XLIW_H_
#define _XLIW_H_

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

/*
 * XLIW: eXtra Long Istruction Word.
 *
 * Each xliw consists of a header, xliw_t, then followed by an 
 * array of xliw_region.  
 *
 * Inside each region are a list of entries.  An entry is always 
 * 4 bytes aligned, with a 4 bytes length header plus binary data, 
 * then 0-filled to 4 bytes alignment.   A region is simply a 
 * sequence of such entries.  
 *
 * For HASH_RESULT or GIS_RESULT, xliw has no region.
 * Just computed result following the header.
 */

typedef enum XLIW_INSTRUCTION
{
	XLIW_I_SHA224, 
	XLIW_I_SHA224_SORT,
	XLIW_I_HASH_RESULT,
	XLIW_I_GIS_INTERSECTS,
	XLIW_I_GIS_RESULT,
} XLIW_INSTRUCTION;

/* 
 * region is a sequence of data payload, packed as part of xliw.
 * region meta data is 16 bytes.
 */
typedef struct xliw_region_t {
	int32_t offset;		/* region start, as offset into xliw */
	int32_t length;		/* length of the region */
	int32_t idx;		/* starting idx. */
	int32_t cnt;		/* number of entries in this region. */
} xliw_region_t;

/*
 * computed hash.  32 bytes. 
 */
typedef struct xliw_hash_t {
	int8_t  sha[28];	/* SHA224, or SHA1 (20 bytes + 8 zero) */
	int32_t idx;	
} xliw_hash_t;

/*
 * XLIW header, 32 bytes.
 */
typedef struct xliw_t {
	int32_t len;				/* total length of xliw */ 
	int32_t xliw;				/* the instruction, see XLIW_T */
	int32_t hcnt;				/* count of entries, total. */ 
	int32_t rcnt;				/* count of regions */ 
	uint64_t chksum[2];			/* chksum, or scratch if not sealed. */
} xliw_t;

static inline xliw_region_t* xliw_region(xliw_t *xliw, int32_t nth) 
{
	xliw_region_t *r0 = (xliw_region_t *) &xliw[1];
	return r0 + nth;
}

static inline int8_t* xliw_region_data(xliw_t *xliw, int32_t nth)
{
	xliw_region_t *r = xliw_region(xliw, nth);
	int8_t *p = (int8_t *) xliw;
	return p + r->offset;
}

static inline xliw_hash_t* xliw_hash(xliw_t *xliw, int32_t nth) 
{
	xliw_hash_t *h0 = (xliw_hash_t *) &xliw[1];
	return h0 + nth;
}

typedef enum XLIW_GIS_TYPE 
{
	XLIW_GIS_POINT, 
	XLIW_GIS_LINE,			/* not used */
	XLIW_GIS_POLYGON, 
} XLIW_GIS_TYPE; 

typedef struct xliw_geom_t {
	int32_t sz;			/* size, in bytes including header. */
	int32_t gt;			/* geometry type. */
	double val[2];		/* double array, [p1.x, p1.y, p2.x, p2.y ...] */
} xliw_geom_t;

/*
 * Execute an XLIW.
 *
 * Execute XLIW, result is putback into same xliw buffer.
 * For HASH or HASH_SORT, each entry will result in one xliw_hash_t.
 * The input xliw must be long enough to hold the computed results.
 *
 * Return >=0 on sucess, where xliw will contain the result of execution.
 * Return <0 on failure.  xliw data will be garbage. 
 */

#define EXEC_ON_CPU 0
#define EXEC_ON_FPGA 1

int xliw_init_fpga(int ndev);
int xliw_deinit_fpga(); 

int xliw_execute(xliw_t *xliw); 
#if 0
int xliw_execute_hash(xliw_t *xliw);
#endif
int xliw_execute_gis_intersects(xliw_t *xliw, int oncpu, int *ts);

/* 
 * code for building xliw
 */
#define XLIW_SZ (16 * 1024 * 1024)
#define XLIW_NREGION (128)

/*
 * Allocating xliw_t.  
 */
xliw_t* xliw_alloc(int32_t xliw_ins);
void xliw_free(xliw_t *xliw);

/*
 * Pack payload data into xliw.
 * return 
 *	>=0 if packed OK.
 * -1 if not enough space.
 */
int xliw_pack_align(xliw_t *xliw, char *payload, int32_t sz, int32_t align);
static inline int xliw_pack(xliw_t* xliw, char* payload, int32_t sz) 
{
	return xliw_pack_align(xliw, payload, sz, 4);
}
static inline int xliw_pack_align8(xliw_t *xliw, char *payload, int32_t sz)
{
	return xliw_pack_align(xliw, payload, sz, 8);
}


/*
 * Seal. 
 */
void xliw_seal(xliw_t *xliw);

/*
 * Utilities.
 */

/* 
 * These are rough estimates.   We will pack a region to approximately
 * up to these limit.   -64 is just a magic number accounting for some 
 * overhead.
 */
static const int32_t XLIW_REGION_X_SZ = (XLIW_SZ / XLIW_NREGION) - 64;
static const int32_t XLIW_REGION_X_CNT = (XLIW_SZ / XLIW_NREGION) / 32; 
static const int32_t XLIW_HCNT_MAX = (XLIW_SZ / 32) - 1;

static inline uint64_t exx_align(uint64_t v, uint64_t align)
{
	return (v + align - 1) & (~(align - 1));
}

static inline bool exx_is_aligned(uint64_t v, uint64_t align)
{
	return exx_align(v, align) == v;
}

static inline int8_t* exx_align_zero(int8_t *p, uint64_t align)
{
	uintptr_t up = (uintptr_t) p;
	uint64_t zsz = 0;
	if (!exx_is_aligned(up, align)) {
		zsz = exx_align(up, align) - up;
		memset(p, 0, zsz);
	}
	return p + zsz;
}

#endif	/* _XLIW_H_ */
