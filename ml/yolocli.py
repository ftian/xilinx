#
# A server running yolov2 on FPGA. 
# Right now, it is a multi threaded server, but each thread locks the device
#
import os, sys, cv2, base64
import threading, multiprocessing

import xdrive_pb2, server

if __name__=='__main__':

    # Test: an echo server.
    if len(sys.argv) != 2:
        raise SystemExit("Usage: yolocli.py addr")

    sock = server.cli_connect(sys.argv[1])
    xmsg = xdrive_pb2.XMsg()
    col1 = xmsg.rowset.columns.add()
    col1.nrow = 1
    col1.nullmap.append(False)
    col1.sdata.append("/home/ftian/BJVideo/24-converted.mp4") 
    col2 = xmsg.rowset.columns.add()
    col2.nrow = 1
    col2.nullmap.append(False)
    col2.f32data.append(0.0) 
    col3 = xmsg.rowset.columns.add()
    col3.nrow = 1
    col3.nullmap.append(False)
    col3.f32data.append(10.0) 

    server.writeXMsg(sock, xmsg)
    ret = server.readXMsg(sock)
    col1 = ret.rowset.columns[0]
    col2 = ret.rowset.columns[1]
    col3 = ret.rowset.columns[2]
    col4 = ret.rowset.columns[3]
    col5 = ret.rowset.columns[4]
    col6 = ret.rowset.columns[5]
    col7 = ret.rowset.columns[6]
    nrow = ret.rowset.columns[0].nrow
    for i in range(nrow):
        print("Frame {0}, Class {1}, Score: {2}\n".format(col1.i32data[i], col2.sdata[i], col3.f32data[i]))
        print("    Bounding Box: [{0}, {1}, {2}, {3}]\n".format(
                    col4.f32data[i],
                    col5.f32data[i],
                    col6.f32data[i],
                    col7.f32data[i]
                    ))
    print("Done!")
