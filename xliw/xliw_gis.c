#include "xliw.h"

/*
 * Input xliw consists of rcnt "regions", each region has a array
 * of "geometry types"
 */

/*
 * Execute GIS_INTERSECTS
 */
int xliw_execute_gis_intersects(xliw_t *xliw)
{
	/* result is just an array of bool */
	int32_t resultsz = exx_align(sizeof(xliw_t) + xliw->hcnt * sizeof(int8_t), 32); 
	xliw_t *result = (xliw_t *) aligned_alloc(32, resultsz); 
	if (!result) {
		/* OOM */
		return -1;
	}
	memset(result, 0, resultsz); 

	result->len = resultsz;
	result->xliw = XLIW_I_GIS_RESULT; 
	result->hcnt = xliw->hcnt;

	/* result is an array of bools, right after result[0] */
	int8_t *retb = (int8_t *)&result[1];

	/* 
	 * Notice the loop, there should be no hazzard at all if we do 
	 * not use a loop, but use rcnt threads instead.
	 */
	for (int i = 0; i < xliw->rcnt; i++) {
		xliw_region_t *r = xliw_region(xliw, i);
		if (r->length == 0) {
			/* empty region */
			continue;
		}

		int8_t *p = xliw_region_data(xliw, i);
		/* process each geometry pair */
		for (int32_t next = 0; next < r->cnt; next++) {
			/* returning idx */ 
			int32_t idx = r->idx + next;

			/* 
			 * decode a pair of geometry type.    First, there is a leading
			 * 8 bytes, which is the total bytes of the two goems.   It is 
			 * redundant, but the old hasher code somehow used this convection.
			 * We keep it.
			 */
			int64_t len = *(int64_t *) p;
			p += sizeof(len);

			xliw_geom_t *geom1 = (xliw_geom_t *) p;
			xliw_geom_t *geom2 = (xliw_geom_t *) (p + geom1->sz);
			p += geom1->sz + geom2->sz;

			/*
			 * Compute intesects function.   Just fake it for now.
			 *
			 * retb[idx] = run_FPGA_intersects_here(geom1, geom2);
			 */
			if (len == geom1->sz + geom2->sz) {
				retb[idx] = 1;
			} else {
				retb[idx] = 0; 
			}
		}
	}
	memcpy(xliw, result, resultsz);
	xliw_free(result);
	return 0;
}

