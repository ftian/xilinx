/*****************************************************************************
 *
 *     Author: Xilinx, Inc.
 *
 *     This text contains proprietary, confidential information of
 *     Xilinx, Inc. , is distributed by under license from Xilinx,
 *     Inc., and may be used, copied and/or disclosed only pursuant to
 *     the terms of a valid license agreement with Xilinx, Inc.
 *
 *     XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"
 *     AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND
 *     SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,
 *     OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,
 *     APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION
 *     THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,
 *     AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE
 *     FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY
 *     WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE
 *     IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR
 *     REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF
 *     INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *     FOR A PARTICULAR PURPOSE.
 *
 *     Xilinx products are not intended for use in life support appliances,
 *     devices, or systems. Use in such applications is expressly prohibited.
 *
 *     (c) Copyright 2018 Xilinx Inc.
 *     All rights reserved.
 *
 *****************************************************************************/

#ifndef XLNX_SHA224_CLUSTER_H
#define XLNX_SHA224_CLUSTER_H
#include <ap_int.h>
#include "sha224.h"
#include "hls_stream.h"

// For debug
#include <cstdio>
#ifndef _DEBUG
#define _DEBUG (0)
#endif
#define _HLS_ALG_VOID_CAST static_cast<void>
// XXX toggle here to debug this file
#define _HLS_ALG_PRINT(msg...) \
  do {                       \
    if (_DEBUG) printf(msg); \
  } while (0)

template <short NPU, int m_width>
static void sha_cluster_dist(hls::stream<ap_uint<m_width> >& msg_strm,        //
                             hls::stream<uint64_t>& len_strm,                 //
                             hls::stream<bool>& end_len_strm,                 //
                             hls::stream<ap_uint<m_width> > msg_strm_pu[NPU], //
                             hls::stream<uint64_t> len_strm_pu[NPU],          //
                             hls::stream<bool> end_len_strm_pu[NPU]) {        //
  const short nbyte = m_width / 8;
  bool dist_end = end_len_strm.read();
  short p = 0;
  short ne = 0;
L_CLUSTER_DIST:
  while (ne != NPU) {
    if (!dist_end) {
      dist_end = end_len_strm.read();
      end_len_strm_pu[p].write(false);
      uint64_t len = len_strm.read();
      len_strm_pu[p].write(len);
      int nword = ((int)len / nbyte) + (((int)len & (nbyte - 1)) != 0);
  L_CLUSTER_DIST_MSG_P:
      for (int i = 0; i < nword; ++i) {
#pragma HLS pipeline II = 1
        ap_uint<m_width> msg = msg_strm.read();
        msg_strm_pu[p].write(msg);
      }
    } else {
      end_len_strm_pu[p].write(true);
      ++ne;
    }
    p = (p == NPU - 1) ? 0 : (p + 1);
  }
}

template <short NPU>
static void sha_cluster_merge(hls::stream<ap_uint<224> > hash_strm_pu[NPU], //
                              hls::stream<bool> end_hash_strm_pu[NPU],      //
                              hls::stream<ap_uint<224> >& hash_strm,        //
                              hls::stream<bool>& end_hash_strm) {           //
  bool merge_end = end_hash_strm_pu[0].read();
  int ne = 0;
  int p = 0;
  int pn = 1;
  while (ne != NPU) {
    if (!merge_end) {
      merge_end = end_hash_strm_pu[pn].read();
      end_hash_strm.write(false);
      ap_uint<224> h = hash_strm_pu[p].read();
      hash_strm.write(h);
    } else {
      end_hash_strm_pu[pn].read();
    }
    ne = merge_end ? (ne + 1) : ne;
    p = pn;
    pn = (pn == NPU - 1) ? 0 : (pn + 1);
  }
  end_hash_strm.write(true);
}

template <short NPU, int m_width>
static void sha224_cluster(hls::stream<ap_uint<m_width> >& msg_strm,
                           hls::stream<uint64_t>& len_strm,
                           hls::stream<bool>& end_len_strm,
                           hls::stream<ap_uint<224> >& hash_strm,
                           hls::stream<bool>& end_hash_strm) {
#pragma HLS dataflow

  hls::stream<ap_uint<m_width> > msg_strm_pu[NPU];
#pragma HLS STREAM variable = msg_strm_pu depth = 64 dim = 1
  hls::stream<uint64_t> len_strm_pu[NPU];
#pragma HLS STREAM variable = len_strm_pu depth = 4 dim = 1
  hls::stream<bool> end_len_strm_pu[NPU];
#pragma HLS STREAM variable = end_len_strm_pu depth = 4 dim = 1

  hls::stream<ap_uint<224> > hash_strm_pu[NPU];
#pragma HLS STREAM variable = hash_strm_pu depth = 4 dim = 1
  hls::stream<bool> end_hash_strm_pu[NPU];
#pragma HLS STREAM variable = end_hash_strm_pu depth = 4 dim = 1

  sha_cluster_dist<NPU>(msg_strm, len_strm, end_len_strm, //
                        msg_strm_pu, len_strm_pu, end_len_strm_pu);

L_CLUSTER_PU_ARRAY_U:
  for (int i = 0; i < NPU; ++i) {
#pragma HLS unroll
    hls_alg::sha224(msg_strm_pu[i], len_strm_pu[i], end_len_strm_pu[i], //
                   hash_strm_pu[i], end_hash_strm_pu[i]);
  }

  sha_cluster_merge<NPU>(hash_strm_pu, end_hash_strm_pu, //
                         hash_strm, end_hash_strm);
}

#undef _HLS_ALG_PRINT
#undef _HLS_ALG_VOID_CAST
#endif // XLNX_SHA224_CLUSTER_H

// -*- cpp -*-
// vim: ts=8:sw=2:sts=2:ft=cpp
