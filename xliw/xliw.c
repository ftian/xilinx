#include "xliw.h"

xliw_t* xliw_alloc(int32_t xliw_ins) 
{
	xliw_t *xliw = aligned_alloc(32, XLIW_SZ); 
	uintptr_t pxliw = (uintptr_t) xliw;

	if (!xliw) {
		return 0;
	}

	int32_t startoffset = sizeof(xliw_t) + XLIW_NREGION * sizeof(xliw_region_t);

	/* zero header.  data is not zeroed. */
	memset(xliw, 0, startoffset); 
	xliw->xliw = xliw_ins;

	/* 
	 * checksum is used as scratch, 
	 *	chksum[0] is the write ptr,  
	 *	chksum[1] is the buffer end;
	 */
	xliw->chksum[0] = pxliw + startoffset; 
	xliw->chksum[1] = pxliw + XLIW_SZ; 

	xliw_region_t *r0 = xliw_region(xliw, 0);
	r0->offset = startoffset;

	return xliw;
}

void xliw_free(xliw_t *xliw)
{
	free(xliw);
}

int xliw_pack_align(xliw_t *xliw, char *payload, int32_t sz, int32_t align)
{
	uint64_t needsz = exx_align(sz, align) + align;
	if (xliw->chksum[0] + needsz > xliw->chksum[1]) {
		return -1;
	}

	if (xliw->hcnt >= XLIW_HCNT_MAX) {
		return -1;
	}

	int8_t *pp;

	if (align == 4) {
		int32_t* p = (int32_t *) xliw->chksum[0];
		*p = sz;
		pp = (int8_t *) &p[1];
	} else {
		int64_t* p = (int64_t *) xliw->chksum[0];
		*p = sz;
		pp = (int8_t *) &p[1];
	}

	memcpy(pp, payload, sz);
	exx_align_zero(pp, align);

	xliw->chksum[0] += needsz;
	xliw->hcnt += 1;

	xliw_region_t *r = xliw_region(xliw, xliw->rcnt);
	r->cnt += 1;
	r->length += needsz;

	if (r->cnt >= XLIW_REGION_X_CNT || r->length >= XLIW_REGION_X_SZ) {
		/* region is full.  switch to next. */
		if (xliw->rcnt + 1 == XLIW_NREGION) { 
			/* xliw is full.  Just return */
			return 0;
		}

		xliw->rcnt += 1;
		r[1].offset = r[0].offset + r[0].length;
		r[1].idx = r[0].idx + r[0].cnt;
	}
	return 1;
}

void xliw_seal(xliw_t *xliw)
{
	uintptr_t pxliw = (uintptr_t) xliw;
	xliw->len = xliw->chksum[0] - pxliw;
	/* seal: current region must be accounted for. */
	xliw->rcnt += 1;

#if 0
	/* 
	 * Maybe it is good idea to compute a checksum -- but too lazy.
	 * leave them as it is.
	 */
	xliw->chksum[0] = ???;
	xliw->chksum[1] = ???;
#endif 
}

int xliw_execute(xliw_t *xliw)
{
	switch (xliw->xliw) {
		case XLIW_I_SHA224:
			return xliw_execute_hash(xliw);
		case XLIW_I_GIS_INTERSECTS:
			return xliw_execute_gis_intersects(xliw);
		default:
			/* bad instruction */
			return -100;
	}
}

