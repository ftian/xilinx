/*****************************************************************************
 *
 *     Author: Xilinx, Inc.
 *
 *     This text contains proprietary, confidential information of
 *     Xilinx, Inc. , is distributed by under license from Xilinx,
 *     Inc., and may be used, copied and/or disclosed only pursuant to
 *     the terms of a valid license agreement with Xilinx, Inc.
 *
 *     XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"
 *     AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND
 *     SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,
 *     OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,
 *     APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION
 *     THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,
 *     AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE
 *     FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY
 *     WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE
 *     IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR
 *     REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF
 *     INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *     FOR A PARTICULAR PURPOSE.
 *
 *     Xilinx products are not intended for use in life support appliances,
 *     devices, or systems. Use in such applications is expressly prohibited.
 *
 *     (c) Copyright 2018 Xilinx Inc.
 *     All rights reserved.
 *
 *****************************************************************************/

#ifndef XLIW_HASHER_H
#define XLIW_HASHER_H

// data type
extern "C" {
#include "xliw.h"
}
#define MAX_MSG_SZ 127

// IO
#define INPUT_SZ 64
#define OUTPUT_SZ (sizeof(xliw_hash_t))

// So cosim knows the depth
#define INPUT_DEPTH (XLIW_SZ / INPUT_SZ)
#define OUTPUT_DEPTH (XLIW_SZ / OUTPUT_SZ + 64)

// kernel
extern "C" {

typedef struct input_s {
  char byte[INPUT_SZ];
} INPUT_T;

typedef struct output_s {
  char byte[OUTPUT_SZ];
} OUTPUT_T;

#ifndef KERNEL_NAME
#define KERNEL_NAME xliw_hasher
#endif
void KERNEL_NAME(INPUT_T xliw_addr[INPUT_DEPTH], OUTPUT_T result_addr[OUTPUT_DEPTH]);
}

#endif
