#ifndef __XCLHOST_H__
#define __XCLHOST_H__

#include <CL/cl.h>
#include <CL/cl_ext.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <new>

#define MSTR_(m) #m
#define MSTR(m) MSTR_(m)

unsigned long read_binary_file(const char* fname, void** buffer);

cl_int init_hardware(cl_context* context, cl_device_id* device_id,
                     cl_command_queue* cmd_queue,
                     cl_command_queue_properties queue_props,
                     const char* dsa_name);

cl_int load_binary(cl_program* program, cl_context context,
                   cl_device_id device_id, const char* xclbin);

#endif // !defined(__XCLHOST_H__)
