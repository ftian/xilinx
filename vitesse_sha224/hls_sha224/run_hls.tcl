open_project -reset "tb.prj"

add_files tb.cpp -cflags "-I../kernel -I../include"
add_files -tb tb.cpp -cflags "-I../kernel -I../include"
set_top hls_db_sha224

open_solution -reset "sol1"

set_part xcvu9p-fsgd2104-2-i
create_clock -period 300MHz -name default
set_clock_uncertainty 27.000000%

config_rtl -register_reset
config_rtl -stall_sig_gen

csim_design -compiler gcc

csynth_design

cosim_design -compiler gcc -trace_level all

exit
