#include <sys/time.h>
#include <stdio.h>

#include "xliw.h"

int tvdiff(struct timeval* tv0, struct timeval *tv1)
{
	return (tv1->tv_sec - tv0->tv_sec) * 1000000 + (tv1->tv_usec - tv0->tv_usec);
}

int main(int argc, char **argv) 
{
	int instruction = XLIW_I_SHA224;
	if (argc > 1 && argv[1][0] == 's') {
		instruction = XLIW_I_SHA224_SORT;
	}

	char data[128];
	for (int i = 0; i < 128; i++) {
		data[i] = i;
	}

	struct timeval tv0, tv1;
	int err = 0;

	int pack_us = 0;
	int exec_us = 0;
	int total_cnt = 0;

	for (int i = 0; i < 10; i++) {
		xliw_t *xliw = xliw_alloc(instruction); 
		gettimeofday(&tv0, 0);
		int cnt;
		for(cnt = 0; xliw_pack(xliw, data, cnt % 128) >= 0; cnt++);
		xliw_seal(xliw);
		gettimeofday(&tv1, 0);
		pack_us += tvdiff(&tv0, &tv1);
		total_cnt += cnt;

		gettimeofday(&tv0, 0);
		err = xliw_execute(xliw);
		if (err < 0) {
			printf("Error!  err code is %d.\n", err);
		}
		gettimeofday(&tv1, 0);
		exec_us += tvdiff(&tv0, &tv1);
		xliw_free(xliw);
	}

	printf("Timing: %d us.  Building 10 XLIW: packed %d datum.\n", pack_us, total_cnt); 
	printf("Timing: %d us.  Executing 10 XLIW: packed %d datum.\n", exec_us, total_cnt); 
	return 0;
}
