#ifndef _XLIWAGENT_H_
#define _XLIWAGENT_H_

#include <stdint.h>
#include <sys/time.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Following are for xliw client/agent solution */
#define XLIW_ERR_SOCK_CREATE		-1
#define XLIW_ERR_SOCK_CONN			-2 
#define XLIW_ERR_CONN_READ			-3 
#define XLIW_ERR_CONN_WRITE			-4
#define XLIW_ERR_CONN_TEARDOWN		-5
#define XLIW_ERR_AGENT				-6
#define XLIW_ERR_NO_MEMORY			-7
#define XLIW_ERR_THREAD_CREATE		-8
#define XLIW_ERR_MUTEX_CREATE		-9
#define XLIW_ERR_SEM_CREATE			-10
#define XLIW_ERR_SEM_WAIT			-11
#define XLIW_ERR_INVALID_REQ_ID		-12

int xliw_connect(char *unix_path);
void xliw_disconnect();
int xliw_execute_agent(xliw_t *xliw, int* exec_us);
int xliw_execute_agent_async(xliw_t *xliw, int *exec_us);
int xliw_future_is_ready(int32_t req_id, bool *is_ready);
int xliw_future_get(int32_t req_id, xliw_t *xliw);

int xliw_agent_start(char *logfile_path,
                     char *unix_path,
                     int  worker_num,
                     int  log_level);
#ifdef __cplusplus
}
#endif

#endif /* _XLIWAGENT_H_ */
