# cluster performance trend

1k entries @ 300Mhz

| Design        | 1k @ 300MHz | BRAM    | LUT      |
| ------------- | ----------: | ------: | -------: |
| 1 pu cluster  |  635.2 usec |         |          |
| 4 pu cluster  |  159.0 usec |         |          |
| 8 pu cluster  |   79.7 usec |  120,2% |   89k,7% |
| 16 pu cluster |   43.5 usec |  240,%5 | 177k,14% |
| 32 pu cluster |   42.4 usec |         |          |

# CPU performance and goal

CPU: ~465 usec / 1k

10x CPU: less than 45 usec / 1k

# plan

16 pu cluster x 2 kernel: expect 23 usec / 1k

OR 8 pu cluster x 4 kernel: ?

# feasible?

input rate: 512b / cycle   

kernel actually processing rate: 64b / cycle / kernel

So one DDR can feed the kernel. 

# how fast can we read?

512b/3.3s ns = 64B/3.33ns

16M / 64 * 3.33 = 0.25M * 3.33ns = 256 K * 3.3ns = 256 * 3.33us = 853 usec

853 usec / 244k = 3.5 usec / 1k entries.

