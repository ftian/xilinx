#define NPP 64
#define PPSZ 19
#define PPNB (PPSZ * 8)

kernel __attribute__((reqd_work_group_size(128, 1, 1)))
void krnl_pip(global const int* pr,
		                 global const double* ppin,
						 global int* retin) 
{
	int idx = get_global_id(0);
	int ppstart = pr[idx*4]; 
	int n_elems = pr[idx*4 + 1]; 
	int retstart = pr[idx*4 + 2]; 
	/* int dummy = pr[idx*4 + 3]; */ 

	global const double *pp = &ppin[ppstart];
	global int *ret = &retin[retstart];
	
	double gbuf[128][NPP * PPSZ];
	int gwn[128][NPP];

	double *buf = gbuf[idx];
	int *wn = gwn[idx];

	for (int n = 0; n < n_elems; n+=NPP) {
		int len = NPP;
		if (n + len > n_elems) {
			len = n_elems - n;
		}

		// COPY IN
		int nstart = n * PPSZ;
		for (int i = 0; i < len * PPSZ; i++) {
			buf[i] = pp[nstart + i];
		}

		// Run PtInPoly for len inputs 
		for (int i = 0; i < len; i++) {
			int ptpos = i * PPSZ + 2;
			int plypos = i * PPSZ + 5;
			double *pt = buf + ptpos; 
			double *ply = buf + plypos;
			wn[i] = 0;

			for (int j = 0; j < 6; j++) {		// edge from v[i] to v[i+1]
				int v0x = j*2;
				int v0y = v0x + 1;
				int v1x = v0y + 1;
				int v1y = v1x + 1;
				if (ply[v0y] <= pt[1]) {		// v0.y <= p.y
					if (ply[v1y] > pt[1]) {		// upward crossing
						double isLeft = 
							(ply[v1x] - ply[v0x]) * (pt[1] - ply[v0y]) 
							- (pt[0] - ply[v0x]) * (ply[v1y] - ply[v0y]); 
						if (isLeft > 0) {
							++wn[i];
						}
					}
				} else {						// v.y > p.y
					if (ply[v1y] <= pt[1]) {
						double isLeft = 
							(ply[v1x] - ply[v0x]) * (pt[1] - ply[v0y]) 
							- (pt[0] - ply[v0x]) * (ply[v1y] - ply[v0y]); 
						if (isLeft < 0) {
							--wn[i];
						}
					}
				}
			}
		}

		for (int i = 0; i < len; i++) { 
			ret[n+i] = wn[i];
		}
	}
}

