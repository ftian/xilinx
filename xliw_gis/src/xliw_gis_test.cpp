#include <stdio.h>
#include "xliw.h"

/* 
 * Basically they are xliw_geom_t, but fix val array size so we don't need to
 * malloc all the time in this toy program.  lazy.
 */
typedef struct xliw_geom_pt_t {
	int32_t sz;			/* size, in bytes including header. */
	int32_t gt;			/* geometry type. */
	double val[2];		/* double array, [p1.x, p1.y, p2.x, p2.y ...] */
} xliw_geom_pt_t;

typedef struct xliw_geom_ply_t {
	int32_t sz;			/* size, in bytes including header. */
	int32_t gt;			/* geometry type. */
	double val[14];		/* double array, [p1.x, p1.y, p2.x, p2.y ...] */
} xliw_geom_ply_t;

typedef struct ptply_t {
	xliw_geom_pt_t pt;
	xliw_geom_ply_t poly;
	char result;
} ptply_t;

typedef struct plyply_t {
	xliw_geom_ply_t poly1;
	xliw_geom_ply_t poly2;
	char result;
} plyply_t;

#define NROW 2000
static ptply_t ptply[NROW]; 
static plyply_t plyply[NROW]; 

#define PTFMT "%lf,%lf"
#define PLYFMT "%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf"
void load_data()
{
	int err = 0;
	FILE *f = fopen("./ptply.txt", "r+");
	for (int i = 0; i < NROW; i++) {
		err = fscanf(f, "{" PTFMT "}|{" PLYFMT "}|%c\n", 
				&ptply[i].pt.val[0], &ptply[i].pt.val[1],
				&ptply[i].poly.val[0], &ptply[i].poly.val[1], &ptply[i].poly.val[2], &ptply[i].poly.val[3],
				&ptply[i].poly.val[4], &ptply[i].poly.val[5], &ptply[i].poly.val[6], &ptply[i].poly.val[7],
				&ptply[i].poly.val[8], &ptply[i].poly.val[9], &ptply[i].poly.val[10], &ptply[i].poly.val[11],
				&ptply[i].poly.val[12], &ptply[i].poly.val[13], 
				&ptply[i].result
			  );
		if (err < 0) {
			printf("Error: reading ptply at line %d.\n", i);
			exit(-100);
		}
		ptply[i].pt.sz = 24;		/* 8 + sizeof(double) * 2 */
		ptply[i].pt.gt = XLIW_GIS_POINT; 
		ptply[i].poly.sz = 120;		/* 8 + sizeof(double) * 14 */
		ptply[i].poly.gt = XLIW_GIS_POLYGON;
	}
	fclose(f);

	f = fopen("./plyply.txt", "r+");

	for (int i = 0; i < NROW; i++) {
		err = fscanf(f, "{" PLYFMT "}|{" PLYFMT "}|%c\n", 
				&plyply[i].poly1.val[0], &plyply[i].poly1.val[1], &plyply[i].poly1.val[2], &plyply[i].poly1.val[3],
				&plyply[i].poly1.val[4], &plyply[i].poly1.val[5], &plyply[i].poly1.val[6], &plyply[i].poly1.val[7],
				&plyply[i].poly1.val[8], &plyply[i].poly1.val[9], &plyply[i].poly1.val[10], &plyply[i].poly1.val[11],
				&plyply[i].poly1.val[12], &plyply[i].poly1.val[13], 
				&plyply[i].poly2.val[0], &plyply[i].poly2.val[1], &plyply[i].poly2.val[2], &plyply[i].poly2.val[3],
				&plyply[i].poly2.val[4], &plyply[i].poly2.val[5], &plyply[i].poly2.val[6], &plyply[i].poly2.val[7],
				&plyply[i].poly2.val[8], &plyply[i].poly2.val[9], &plyply[i].poly2.val[10], &plyply[i].poly2.val[11],
				&plyply[i].poly2.val[12], &plyply[i].poly2.val[13], 
				&plyply[i].result
			  );

		if (err < 0) {
			printf("Error: reading plyply at line %d.\n", i);
			exit(-100);
		}
		plyply[i].poly1.sz = 120;		/* 8 + sizeof(double) * 14 */
		plyply[i].poly1.gt = XLIW_GIS_POLYGON;
		plyply[i].poly2.sz = 120;		/* 8 + sizeof(double) * 14 */
		plyply[i].poly2.gt = XLIW_GIS_POLYGON;
	}
	fclose(f);


	/* Simple check */
	int nt = 0;
	int nf = 0;
	for (int i = 0; i < NROW; i++) {
		if (ptply[i].result == 't') {
			nt++;
		}
		if (ptply[i].result == 'f') {
			nf++;
		}
	}
	printf("Ptply: nt %d, nf %d, tot %d\n", nt, nf, nt+nf);


	nt = 0;
	nf = 0;
	for (int i = 0; i < NROW; i++) {
		if (plyply[i].result == 't') {
			nt++;
		}
		if (plyply[i].result == 'f') {
			nf++;
		}
	}
	printf("Plyply: nt %d, nf %d, tot %d\n", nt, nf, nt+nf);
}

void test_ptply(int execon, int *us) 
{
	xliw_t *xliw = xliw_alloc(XLIW_I_GIS_INTERSECTS);
	int err = 0;

	for (int i = 0; i < NROW; i++) {
		err = xliw_pack_align8(xliw, (char *)&ptply[i], 24+120);
		if (err <= 0) {
			printf("test_ptply cannot packin %d-th pair.\n", i);
			exit(-10);
		}
	}
	xliw_seal(xliw);
	err = xliw_execute_gis_intersects(xliw, execon, us);

	if (err < 0 || xliw->xliw != XLIW_I_GIS_RESULT || xliw->hcnt != NROW) {
		printf("test_ptply failed.\n"); 
		exit(-10); 
	}

	int *retb = (int*) &xliw[1];
	for (int i = 0; i < NROW; i++) {
		bool chk = (ptply[i].result == 't');
		bool chk2 = retb[i] != 0;
		if (chk != chk2) {
			printf("Error: ptply test failed, result %d (%c != %d).\n", i, ptply[i].result, retb[i]);
			exit(-10);
		}
	}

	xliw_free(xliw);
}

void test_ptply2(int execon, int *us) 
{
	xliw_t *xliw = xliw_alloc(XLIW_I_GIS_INTERSECTS);
	int err = 0;
	int full = 0;
	int n = 0;

	while (!full) {
		for (int i = 0; i < NROW; i++) {
			err = xliw_pack_align8(xliw, (char *)&ptply[i], 24+120);
			if (err < 0) {
				full = 1; 
				printf("test_ptply2.  Packed full, total %d pairs.\n", n);
				break;
			}
			n += 1;
		}
	}

	xliw_seal(xliw);
	err = xliw_execute_gis_intersects(xliw, execon, us);

	if (err < 0 || xliw->xliw != XLIW_I_GIS_RESULT || xliw->hcnt != n) { 
		printf("test_ptply2 failed.\n"); 
		exit(-10); 
	}

	int *retb = (int*) &xliw[1];
	n = 0; 
	while (n < xliw->hcnt) {
		for (int i = 0; i < NROW; i++) {
			if (n < xliw->hcnt) { 
				bool chk = (ptply[i].result == 't');
				bool chk2 = retb[n] != 0;
				if (chk != chk2) {
					printf("Error: ptply test failed, result %d (%c != %d).\n", n, ptply[i].result, retb[n]);
					// exit(-10);
				}
				n++;
			} else {
				break;
			}
		}
	}

	xliw_free(xliw);
}


int main()
{
	load_data();

	xliw_init_fpga(1);

	/* Test 3 cases.   ptply, plyply, and mix/match till xliw is filled. */
	int us = 0;
	test_ptply(EXEC_ON_CPU, &us);
	printf("Testing on CPU.   PtPly used %d us.\n", us);

	test_ptply2(EXEC_ON_CPU, &us);
	printf("Testing on CPU.   PtPly2 used %d us.\n", us);

	test_ptply(EXEC_ON_FPGA, &us);
	printf("Testing on FPGA.   PtPly used %d us.\n", us);

	test_ptply2(EXEC_ON_FPGA, &us);
	printf("Testing on FPGA.   PtPly2 used %d us.\n", us);

	printf("Testing OK!\n");

	xliw_deinit_fpga();
}
