#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <vector>

#include "xliw.h"
#include "xliwagent.h"

static void print_options()
{
    printf("Valid Options\n");
    printf("    -iter <num>\n");
    printf("            Specify the number of iterations.\n");
}

int main(int argc, char* argv[])
{
    int i = 1;
    int iter = 10;

    while(i < argc) {
        char *option = argv[i++];

        if( 0 == strcmp(option, "-iter")) {
            iter = atoi(argv[i++]);
        } else {
            // invalid option
            print_options();
            return -1;
        }
    }

	char data[128];
	for (int i = 0; i < 128; i++) {
		data[i] = i;
	}

	int exec_us = 0;
	int total_cnt = 0;

	std::vector<xliw_t*> cpu_xliws(iter);
	for (int i = 0; i < iter; i++) {
		xliw_t *xliw = xliw_alloc(XLIW_I_SHA224);
		cpu_xliws[i] = xliw;
		int cnt;
		for (cnt = 0; cnt < 2000000 && xliw_pack(xliw, data, cnt % 128) > 0; cnt++)
			;
		xliw_seal(xliw);
		total_cnt += xliw->hcnt;
	}

	for (int i = 0; i < iter; i++) {
		xliw_t *xliw = cpu_xliws[i];
		int usec = 0;
		int err = xliw_execute_cpu(xliw, &usec);
		if (err < 0) {
			printf("xliw_exec_cpu failed.\n");
			return -2;
		}
		exec_us += usec;
	}
	printf("CPU Timing: %d us.  %d iter, %d hashes.\n", exec_us, iter, total_cnt);

	
    int ret = xliw_connect((char *) "/tmp/xliwagent.socket");
    if( 0 != ret) {
        printf("xliw_connect failed with error %d\n", ret);
	}

	total_cnt = 0;
	exec_us = 0;
	std::vector<xliw_t*> cli_xliws(iter);
	for (int i = 0; i < iter; i++) {
		xliw_t *xliw = xliw_alloc(XLIW_I_SHA224);
		cli_xliws[i] = xliw;
		int cnt;
		for (cnt = 0; cnt < 2000000 && xliw_pack(xliw, data, cnt % 128) > 0; cnt++)
			;
		xliw_seal(xliw);
		total_cnt += xliw->hcnt;
	}


    for(i=0; i<iter; i++) {
		xliw_t *xliw = cli_xliws[i];
		int usec = 0;
        ret = xliw_execute_agent(xliw, &usec); 
        if(0 != ret || xliw->xliw != XLIW_I_HASH_RESULT) {
            printf("xliw_execute_agent failed with error %d\n", ret);
        } 
		exec_us += usec;
    }
	printf("CLI Timing: %d us.  %d iter, %d hashes.\n", exec_us, iter, total_cnt);
	
	// Verify.
	for (int i = 0; i < iter; i++) {
		xliw_t *cx = cpu_xliws[i];
		xliw_t *fx = cli_xliws[i];

		if (cx->hcnt != fx->hcnt) {
			printf("Result %d: CPU cnt %d, CLI cnt %d.   MISMATCH!!!\n", i, cx->hcnt, fx->hcnt);
			return -3;
		}

		for (int j =- 0; j < cx->hcnt; j++) {
			xliw_hash_t *ch = xliw_hash(cx, j);
			xliw_hash_t *fh = xliw_hash(fx, j);
			if (memcmp(ch, fh, sizeof(xliw_hash_t)) != 0) {
				printf("Result %d, %d: hash mismatch!!!\n    CPU: ", i, j);
				for (int k = 0; k<28; k++) {
					printf("%02x", (unsigned char) ch->sha[k]);
				}
				printf("\n    CLI: "); 
				for (int k = 0; k<28; k++) {
					printf("%02x", (unsigned char) fh->sha[k]);
				}
				return -4;
			}
		}
	}

    xliw_disconnect();
    return 0;
}

